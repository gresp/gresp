module kleos.unice.fr/peix/gresp

go 1.18

require (
	kleos.unice.fr/peix/argo v0.1.2
	kleos.unice.fr/peix/gdbird v1.2.21
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20190905194746-02993c407bfb // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
	kleos.unice.fr/peix/gseed v1.2.7 // indirect
	kleos.unice.fr/peix/simpleyaml v1.0.0 // indirect
	kleos.unice.fr/peix/stationxml v1.1.16 // indirect
)

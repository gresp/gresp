package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// Location is ydbird Location implementation
type Location struct {
	longitude  dbirdv2.UncertaintyFloat64
	latitude   dbirdv2.UncertaintyFloat64
	elevation  dbirdv2.UncertaintyFloat64
	waterLevel *float64
	code       string
	vault      string
	geology    string
	depth      float64
}

// Code is dbird interface
func (l Location) Code() string {
	return l.code
}

// Longitude is dbird interface
func (l Location) Longitude() dbirdv2.UncertaintyFloat64 {
	return l.longitude
}

// Latitude is dbird interface
func (l Location) Latitude() dbirdv2.UncertaintyFloat64 {
	return l.latitude
}

// Elevation is dbird interface
func (l Location) Elevation() dbirdv2.UncertaintyFloat64 {
	return l.elevation
}

// Depth is dbird interface
func (l Location) Depth() float64 {
	return l.depth
}

// WaterLevel is dbird interface
func (l Location) WaterLevel() dbirdv2.UncertaintyFloat64 {
	if l.waterLevel == nil {
		return nil
	}
	return &UncertaintyFloat64{
		value:      *(l.waterLevel),
		plusError:  nil,
		minusError: nil,
	}
}

// Vault is dbird interface
func (l Location) Vault() string {
	return l.vault
}

// Geology is dbird interface
func (l Location) Geology() string {
	return l.geology
}

// NewLocation create a new dbirdv2.Location from YAML representation.
func NewLocation(location *simpleyaml.Yaml) (result dbirdv2.Location, err error) {
	code, lerr := location.GetIndex(0).String()
	if lerr != nil {
		err = fmt.Errorf("error during location construction: %s", lerr.Error())
		return
	}
	latitude, lerr := NewUncertaintyFloat64(location.GetIndex(1))
	if lerr != nil {
		err = fmt.Errorf("error during location construction (latitude): %s", lerr.Error())
		return
	}
	longitude, lerr := NewUncertaintyFloat64(location.GetIndex(2))
	if lerr != nil {
		err = fmt.Errorf("error during location construction (longitude): %s", lerr.Error())
		return
	}
	elevation, lerr := NewUncertaintyFloat64(location.GetIndex(3))
	if lerr != nil {
		err = fmt.Errorf("error during location construction (elevation): %s", lerr.Error())
		return
	}
	depth, lerr := location.GetIndex(4).Float()
	if lerr != nil {
		err = fmt.Errorf("error during location construction (depth): %s", lerr.Error())
		return
	}
	var waterLevelP *float64
	tmpYaml := location.GetIndex(5)
	if tmpYaml.IsFound() && !tmpYaml.IsNil() {
		waterLevel, lerr := tmpYaml.Float()
		if lerr != nil {
			err = fmt.Errorf("error during location construction (water level): %s", lerr.Error())
			return
		} else {
			waterLevelP = &waterLevel
		}
	}

	vault, lerr := location.GetIndex(6).String()
	if lerr != nil {
		err = fmt.Errorf("error during location construction (vault): %s", lerr.Error())
		return
	}
	geology, lerr := location.GetIndex(7).String()
	if lerr != nil {
		err = fmt.Errorf("error during location construction (geology): %s", lerr.Error())
	}
	result = Location{
		code:       code,
		latitude:   latitude,
		longitude:  longitude,
		elevation:  elevation,
		depth:      depth,
		waterLevel: waterLevelP,
		vault:      vault,
		geology:    geology}
	return
}

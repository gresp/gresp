package ydbird

import (
	"fmt"
	"time"

	"kleos.unice.fr/peix/simpleyaml"
)

const (
	//DateFormat define ydbird datetime format
	DateFormat = "2006-01-02T15:04:05Z"
)

func stringSlice(yaml *simpleyaml.Yaml, nbElt int) (result []string, err error) {
	var size int

	if nbElt == 0 {
		size, err = yaml.GetArraySize()
		if err != nil {
			return
		}
	} else {
		size = nbElt
	}
	result = make([]string, size)
	for i := 0; i < size; i++ {
		result[i], err = yaml.GetIndex(i).String()
		if err != nil {
			return
		}
	}
	return
}

func timeFromString(strTime string) (result *time.Time, err error) {
	if strTime == "present" {
		return
	}
	r, lerr := time.Parse(DateFormat, strTime)
	if lerr != nil {
		err = fmt.Errorf("can't parse datetime %s: %s", strTime, lerr.Error())
		return
	}
	result = &r
	return
}

package ydbird

import (
	"fmt"
	"time"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// Comment is YAML implementation of dbird interface
type Comment struct {
	subject     string
	description string
	startTime   *time.Time
	endTime     *time.Time
	cType       string
	author      []dbirdv2.Author
}

// Subject implement dbird.Comment interface
func (c Comment) Subject() string {
	return c.subject
}

// Description implement dbird.Comment interface
func (c Comment) Description() string {
	return c.description
}

// StartTime implement dbird.Comment interface
func (c Comment) StartTime() *time.Time {
	return c.startTime
}

// EndTime implement dbird.Comment interface
func (c Comment) EndTime() *time.Time {
	return c.endTime
}

// Authors implement dbird.Comment interface
func (c Comment) Authors() []dbirdv2.Author {
	return c.author
}

// Type implement dbird.Comment interface
func (c Comment) Type() string {
	return c.cType
}

// NewComment create a Comment from a given YAML
// [ "Comment subject", "DOI : https://doi.org/10.15778/RESIF.RA", "1995-01-01T00:00:00Z", "present",
//   [ *AUTH2 ] "N"]
func NewComment(comment *simpleyaml.Yaml) (result Comment, err error) {
	var nbAuthor int
	commentDef, lerr := stringSlice(comment, 4)
	if lerr != nil {
		err = fmt.Errorf("error during comment creation: %s", lerr.Error())
		return
	}
	authorList := comment.GetIndex(4)
	nbAuthor, lerr = authorList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during comment creation: %s", lerr.Error())
		return
	}

	commentAuthor := make([]dbirdv2.Author, nbAuthor)
	for i := 0; i < nbAuthor; i++ {
		currentYamlAuthor := authorList.GetIndex(i)
		commentAuthor[i], lerr = NewAuthor(currentYamlAuthor)
		if lerr != nil {
			err = fmt.Errorf("error during comment creation: %s", lerr.Error())
			return
		}
	}
	cType, lerr := comment.GetIndex(5).String()
	if lerr != nil {
		err = fmt.Errorf("error during comment creation: %s", lerr.Error())
		return
	}
	startTime, lerr := timeFromString(commentDef[2])
	if lerr != nil {
		err = fmt.Errorf("error during comment creation (starttime): %s", lerr.Error())
		return
	}
	endTime, lerr := timeFromString(commentDef[3])
	if lerr != nil {
		err = fmt.Errorf("error during comment creation (endtime): %s", lerr.Error())
		return
	}
	result = Comment{
		subject:     commentDef[0],
		description: commentDef[1],
		startTime:   startTime,
		endTime:     endTime,
		cType:       cType,
		author:      commentAuthor}
	return
}

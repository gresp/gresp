package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

//
// UncertaintyFloat64 define float64 value with uncertainty
//
type UncertaintyFloat64 struct {
	plusError  *float64
	minusError *float64
	value      float64
}

// Value is dbird interface
func (uf UncertaintyFloat64) Value() float64 {
	return uf.value
}

// PlusError is dbird interface
func (uf UncertaintyFloat64) PlusError() *float64 {
	return uf.plusError
}

// MinusError is dbird interface
func (uf UncertaintyFloat64) MinusError() *float64 {
	return uf.minusError
}

// NewUncertaintyFloat64 create a new UncertaintyFloat from YAML uncertainty float
func NewUncertaintyFloat64(ufloat *simpleyaml.Yaml) (result dbirdv2.UncertaintyFloat64, err error) {
	value, lerr := ufloat.GetIndex(0).Float()
	if lerr != nil {
		err = fmt.Errorf("error creating UncertaintyFloat64: %s", lerr.Error())
		return
	}
	var plusError, minusError *float64
	var tmpFloat1, tmpFloat2 float64
	tmpYaml := ufloat.GetIndex(1)
	if tmpYaml.IsFound() {
		tmpFloat1, lerr = tmpYaml.Float()
		if lerr != nil {
			err = fmt.Errorf("error creating UncertaintyFloat64: %s", lerr.Error())
			return
		}
		plusError = &tmpFloat1
	}
	tmpYaml = ufloat.GetIndex(2)
	if tmpYaml.IsFound() {
		tmpFloat2, lerr = tmpYaml.Float()
		if lerr != nil {
			err = fmt.Errorf("error creating UncertaintyFloat64: %s", lerr.Error())
			return
		}
		minusError = &tmpFloat2
	}
	result = UncertaintyFloat64{
		value:      value,
		plusError:  plusError,
		minusError: minusError}
	return
}

package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// DigitalFilter define ydbird representation of DigitalFilter
type DigitalFilter struct {
	dfType       string
	serialNumber string
	pzFile       string
}

// Type is dbird interface
func (df DigitalFilter) Type() string {
	return df.dfType
}

// SerialNumber is dbird interface
func (df DigitalFilter) SerialNumber() string {
	return df.serialNumber
}

// PZFile is dbird interface
func (df DigitalFilter) PZFile() string {
	return df.pzFile
}

//
// NewDigitalFilter create a dbirdv2.DigitalFilter from YAML representation.
//
func NewDigitalFilter(digitalFilter *simpleyaml.Yaml) (result dbirdv2.DigitalFilter, err error) {
	filterDef, lerr := stringSlice(digitalFilter, 0)
	if lerr != nil {
		err = fmt.Errorf("error during digital filter creation: %s", lerr.Error())
		return
	}
	result = DigitalFilter{
		dfType:       filterDef[0],
		serialNumber: filterDef[1],
		pzFile:       filterDef[2]}
	return
}

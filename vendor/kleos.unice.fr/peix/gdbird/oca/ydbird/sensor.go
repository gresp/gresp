package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// Sensor is ydbird Sensor implementation
type Sensor struct {
	azimuth      *float64
	dip          *float64
	stype        string
	serialNumber string
	pzFile       string
}

// Type is dbiurd interface
func (s Sensor) Type() string {
	return s.stype
}

// SerialNumber is dbiurd interface
func (s Sensor) SerialNumber() string {
	return s.serialNumber
}

// PZFile is dbiurd interface
func (s Sensor) PZFile() string {
	return s.pzFile
}

// Azimuth is dbiurd interface
func (s Sensor) Azimuth() *float64 {
	return s.azimuth
}

// Dip is dbiurd interface
func (s Sensor) Dip() *float64 {
	return s.dip
}

// NewSensor create a dbirdv2.Sensor from YAML sensor
func NewSensor(sensor *simpleyaml.Yaml) (result dbirdv2.Sensor, err error) {
	sensorDef, lerr := stringSlice(sensor, 3)
	if lerr != nil {
		err = fmt.Errorf("error creating sensor %s", lerr.Error())
		return
	}
	var azimuth, dip *float64
	if !sensor.GetIndex(3).IsNil() {
		tmp, lerr := sensor.GetIndex(3).Float()
		if lerr != nil {
			err = fmt.Errorf("error creating sensor %s", lerr.Error())
			return
		}
		azimuth = &tmp
	}
	if !sensor.GetIndex(4).IsNil() {
		tmp, lerr := sensor.GetIndex(4).Float()
		if lerr != nil {
			err = fmt.Errorf("error creating sensor %s", lerr.Error())
			return
		}
		dip = &tmp
	}
	result = Sensor{
		stype:        sensorDef[0],
		serialNumber: sensorDef[1],
		pzFile:       sensorDef[2],
		azimuth:      azimuth,
		dip:          dip}
	return
}

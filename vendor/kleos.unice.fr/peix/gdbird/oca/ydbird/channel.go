package ydbird

import (
	"fmt"
	"time"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

//
// Channel define channel description
//
type Channel struct {
	digitizer        dbirdv2.Digitizer
	location         dbirdv2.Location
	sensor           dbirdv2.Sensor
	network          dbirdv2.Network
	startTime        *time.Time
	endTime          *time.Time
	flags            string
	irisCode         string
	restrictedStatus string
	encoding         string
	analogFilter     []dbirdv2.AnalogFilter
	digitalFilter    []dbirdv2.DigitalFilter
	comments         []dbirdv2.Comment
	sampleRate       float64
}

// Location is dbird interface
func (c Channel) Location() dbirdv2.Location {
	return c.location
}

// IrisCode is dbird interface
func (c Channel) IrisCode() string {
	return c.irisCode
}

// SampleRate is dbird interface
func (c Channel) SampleRate() float64 {
	return c.sampleRate
}

// Sensor is dbird interface
func (c Channel) Sensor() dbirdv2.Sensor {
	return c.sensor
}

// AnalogFilter is dbird interface
func (c Channel) AnalogFilter() []dbirdv2.AnalogFilter {
	return c.analogFilter
}

// Digitizer is dbird interface
func (c Channel) Digitizer() dbirdv2.Digitizer {
	return c.digitizer
}

// DigitalFilter is dbird interface
func (c Channel) DigitalFilter() []dbirdv2.DigitalFilter {
	return c.digitalFilter
}

// Flags is dbird interface
func (c Channel) Flags() string {
	return c.flags
}

// Encoding is dbird interface
func (c Channel) Encoding() string {
	return c.encoding
}

// StartTime is dbird interface
func (c Channel) StartTime() *time.Time {
	return c.startTime
}

// EndTime is dbird interface
func (c Channel) EndTime() *time.Time {
	return c.endTime
}

// Network is dbird interface
func (c Channel) Network() dbirdv2.Network {
	return c.network
}

// Comments is dbird interface
func (c Channel) Comments() []dbirdv2.Comment {
	return c.comments
}

// RestrictedStatus is dbird interface
func (c Channel) RestrictedStatus() string {
	return c.restrictedStatus
}

func getAnalogFilter(analogFilterList *simpleyaml.Yaml) (result []dbirdv2.AnalogFilter, err error) {
	nbFilter, lerr := analogFilterList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during analog filter creation: %s", lerr.Error())
		return
	}

	channelAnalogFilter := make([]dbirdv2.AnalogFilter, nbFilter)
	for i := 0; i < nbFilter; i++ {
		currentYamlAnalogFilter := analogFilterList.GetIndex(i)
		channelAnalogFilter[i], lerr = NewAnalogFilter(currentYamlAnalogFilter)
		if lerr != nil {
			err = fmt.Errorf("error during analog filter creation: %s", lerr.Error())
			return
		}
	}
	result = channelAnalogFilter
	return
}

func getDigitalFilter(digitalFilterList *simpleyaml.Yaml) (result []dbirdv2.DigitalFilter, err error) {
	nbFilter, lerr := digitalFilterList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during digital filter creation: %s", lerr.Error())
		return
	}

	channelDigitalFilter := make([]dbirdv2.DigitalFilter, nbFilter)
	for i := 0; i < nbFilter; i++ {
		currentYamlAnalogFilter := digitalFilterList.GetIndex(i)
		channelDigitalFilter[i], lerr = NewDigitalFilter(currentYamlAnalogFilter)
		if lerr != nil {
			err = fmt.Errorf("error during digital filter creation: %s", lerr.Error())
			return
		}
	}
	result = channelDigitalFilter
	return
}

func getComment(commentList *simpleyaml.Yaml) (result []dbirdv2.Comment, err error) {
	nbComment, lerr := commentList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during network creation: %s", lerr.Error())
		return
	}

	channelComment := make([]dbirdv2.Comment, nbComment)
	for i := 0; i < nbComment; i++ {
		currentYamlComment := commentList.GetIndex(i)
		channelComment[i], lerr = NewComment(currentYamlComment)
		if lerr != nil {
			err = fmt.Errorf("error during network creation: %s", lerr.Error())
			return
		}
	}
	result = channelComment
	return
}

//
// NewChannel create a dbird.Channel from YAML representation.
//
func NewChannel(channel *simpleyaml.Yaml) (result dbirdv2.Channel, err error) {
	location, lerr := NewLocation(channel.GetIndex(0))
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	irisCode, lerr := channel.GetIndex(1).String()
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	sensor, sensorErr := NewSensor(channel.GetIndex(2))
	if sensorErr != nil {
		err = sensorErr
		return
	}
	analogFilter, lerr := getAnalogFilter(channel.GetIndex(3))
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	digitizer, lerr := NewDigitizer(channel.GetIndex(4))
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	digitalFilter, lerr := getDigitalFilter(channel.GetIndex(5))
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	flags, lerr := channel.GetIndex(6).String()
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	encoding, lerr := channel.GetIndex(7).String()
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	startTimeStr, lerr := channel.GetIndex(8).String()
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	startTime, lerr := timeFromString(startTimeStr)
	if lerr != nil {
		err = fmt.Errorf("error during channel construction (starttime) %s", lerr.Error())
		return
	}
	endTimeStr, lerr := channel.GetIndex(9).String()
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}
	endTime, lerr := timeFromString(endTimeStr)
	if lerr != nil {
		err = fmt.Errorf("error during channel construction (endtime) %s", lerr.Error())
		return
	}

	sampleRate, lerr := channel.GetIndex(10).Float()
	if lerr != nil {
		err = fmt.Errorf("error during channel construction %s", lerr.Error())
		return
	}

	network, lerr := NewNetwork(channel.GetIndex(11))
	if lerr != nil {
		err = fmt.Errorf("error creating network: %s", lerr.Error())
		return
	}
	comments, lerr := getComment(channel.GetIndex(12))
	if lerr != nil {
		err = fmt.Errorf("error getting restrictedStatus: %s", lerr.Error())
		return
	}
	restrictedStatus, lerr := channel.GetIndex(13).String()
	if lerr != nil {
		err = fmt.Errorf("error getting restrictedStatus: %s", lerr.Error())
		return
	}

	result = Channel{
		location:         location,
		irisCode:         irisCode,
		sensor:           sensor,
		analogFilter:     analogFilter,
		digitizer:        digitizer,
		digitalFilter:    digitalFilter,
		flags:            flags,
		encoding:         encoding,
		startTime:        startTime,
		endTime:          endTime,
		sampleRate:       sampleRate,
		network:          network,
		restrictedStatus: restrictedStatus,
		comments:         comments}
	return
}

package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// AnalogFilter is ydbird AnalogFilter implemenation
type AnalogFilter struct {
	afType       string
	serialNumber string
	pzFile       string
}

// Type is dbird interface
func (af AnalogFilter) Type() string {
	return af.afType
}

// SerialNumber is dbird interface
func (af AnalogFilter) SerialNumber() string {
	return af.serialNumber
}

// PZFile is dbird interface
func (af AnalogFilter) PZFile() string {
	return af.pzFile
}

// NewAnalogFilter create dbirdv2.AnalogFilter from YAML representation
func NewAnalogFilter(analogFilter *simpleyaml.Yaml) (result dbirdv2.AnalogFilter, err error) {
	filterDef, lerr := stringSlice(analogFilter, 0)
	if lerr != nil {
		err = fmt.Errorf("errror during analog filter creation: %s", lerr.Error())
		return
	}
	result = AnalogFilter{
		afType:       filterDef[0],
		serialNumber: filterDef[1],
		pzFile:       filterDef[2]}
	return
}

package ydbird

import (
	"fmt"
	"time"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// Station is the ydbird implementation of dbird interface
type Station struct {
	site             dbirdv2.Site
	longitude        dbirdv2.UncertaintyFloat64
	latitude         dbirdv2.UncertaintyFloat64
	elevation        dbirdv2.UncertaintyFloat64
	waterLevel       *float64
	startTime        *time.Time
	endTime          *time.Time
	pzRoot           string
	irisCode         string
	historicalCode   string
	alternateCode    string
	restrictedStatus string
	comments         []dbirdv2.Comment
	channels         []dbirdv2.Channel
	observatory      []dbirdv2.Observatory
}

// IrisCode is dbird interface
func (s Station) IrisCode() string {
	return s.irisCode
}

// StartTime is dbird interface
func (s Station) StartTime() *time.Time {
	return s.startTime
}

// EndTime is dbird interface
func (s Station) EndTime() *time.Time {
	return s.endTime
}

// Longitude is dbird interface
func (s Station) Longitude() dbirdv2.UncertaintyFloat64 {
	return s.longitude
}

// Latitude is dbird interface
func (s Station) Latitude() dbirdv2.UncertaintyFloat64 {
	return s.latitude
}

// Elevation is dbird interface
func (s Station) Elevation() dbirdv2.UncertaintyFloat64 {
	return s.elevation
}

// WaterLevel is dbird interface
func (s Station) WaterLevel() dbirdv2.UncertaintyFloat64 {
	if s.waterLevel == nil {
		return nil
	}
	return &UncertaintyFloat64{
		value:      *(s.waterLevel),
		plusError:  nil,
		minusError: nil,
	}
}

// Site is dbird interface
func (s Station) Site() dbirdv2.Site {
	return s.site
}

// Observatory is dbird interface
func (s Station) Observatory() []dbirdv2.Observatory {
	return s.observatory
}

// Comments is dbird interface
func (s Station) Comments() (result []dbirdv2.Comment) {
	return s.comments
}

// Channels is dbird interface
func (s Station) Channels() (result []dbirdv2.Channel) {
	return s.channels
}

// RestrictedStatus is dbird interface
func (s Station) RestrictedStatus() string {
	return s.restrictedStatus
}

// AlternateCode is dbird interface
func (s Station) AlternateCode() string {
	return s.alternateCode
}

// HistoricalCode is dbird interface
func (s Station) HistoricalCode() string {
	return s.historicalCode
}

// PzRoot is dbird interface
func (s Station) PzRoot() string {
	return s.pzRoot
}

func getStationComment(commentList *simpleyaml.Yaml) (result []dbirdv2.Comment, err error) {
	nbComment, lerr := commentList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error creating station comments: %s", lerr.Error())
		return
	}

	stationComment := make([]dbirdv2.Comment, 0)
	for i := 0; i < nbComment; i++ {
		currentYamlComment := commentList.GetIndex(i)

		currentComment, lerr := NewComment(currentYamlComment)
		if lerr != nil {
			err = fmt.Errorf("error creating station comments: %s", lerr.Error())
			return
		}
		if currentComment.Type() == "S" {
			stationComment = append(stationComment, currentComment)
		}
	}
	result = stationComment
	return
}

// NewStation create a new Station from YAML station
func NewStation(station *simpleyaml.Yaml, pzRoot string) (result dbirdv2.Station, err error) {
	stationDefinition := station.Get("definition")
	stationDef, lerr := stringSlice(stationDefinition, 3)
	if lerr != nil {
		err = fmt.Errorf("error during station creation: %s", lerr.Error())
		return
	}
	latitude, lerr := NewUncertaintyFloat64(stationDefinition.GetIndex(3))
	if lerr != nil {
		err = fmt.Errorf("error during station creation (latitude): %s", lerr.Error())
		return
	}
	longitude, lerr := NewUncertaintyFloat64(stationDefinition.GetIndex(4))
	if lerr != nil {
		err = fmt.Errorf("error during station creation (longitude): %s", lerr.Error())
		return
	}
	elevation, lerr := NewUncertaintyFloat64(stationDefinition.GetIndex(5))
	if lerr != nil {
		err = fmt.Errorf("error during station creation (elevation): %s", lerr.Error())
		return
	}
	var waterLevelP *float64
	tmpYaml := stationDefinition.GetIndex(6)
	if tmpYaml.IsFound() && !tmpYaml.IsNil() {
		waterLevel, lerr := tmpYaml.Float()
		if lerr != nil {
			err = fmt.Errorf("error during station construction (water level): %s", lerr.Error())
			return
		} else {
			waterLevelP = &waterLevel
		}
	}

	restrictedStatus, lerr := stationDefinition.GetIndex(8).String()
	if lerr != nil {
		err = fmt.Errorf("error getting restrictedStatus (restricted status): %s", lerr.Error())
		return
	}
	alternateCode, lerr := stationDefinition.GetIndex(9).String()
	if lerr != nil {
		err = fmt.Errorf("error getting alternate code (alternate code): %s", lerr.Error())
		return
	}
	historicalCode, lerr := stationDefinition.GetIndex(10).String()
	if lerr != nil {
		err = fmt.Errorf("error getting historical code (historical code): %s", lerr.Error())
		return
	}
	observatories := station.Get("observatories")
	var nbObservatory int
	nbObservatory, lerr = observatories.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error getting observatories array size: %s", lerr)
		return
	}

	var observatoryList []dbirdv2.Observatory
	for i := 0; i < nbObservatory; i++ {
		newObs, lerr := NewObservatory(observatories.GetIndex(i))
		if lerr != nil {
			err = fmt.Errorf("error creating observatory: %s", lerr.Error())
			return
		}
		observatoryList = append(observatoryList, newObs)
	}
	site := station.Get("site")
	if site == nil {
		err = fmt.Errorf("can't get site for %s", stationDef[0])
		return
	}
	var siteName, siteDescription, siteTown, siteCounty, siteRegion, siteCountry string
	siteName, lerr = site.Get("name").String()
	if lerr != nil {
		err = fmt.Errorf("error during station creation (site name): %s", lerr.Error())
		return
	}
	if site.Get("description").IsFound() {
		siteDescription, lerr = site.Get("description").String()
		if lerr != nil {
			err = fmt.Errorf("error during station creation (site description): %s", lerr.Error())
			return
		}
	}
	if site.Get("town").IsFound() {
		siteTown, lerr = site.Get("town").String()
		if lerr != nil {
			err = fmt.Errorf("error during station creation (site town): %s", lerr.Error())
			return
		}
	}

	if site.Get("county").IsFound() {
		siteCounty, lerr = site.Get("county").String()
		if lerr != nil {
			err = fmt.Errorf("error during station creation (site county): %s", lerr.Error())
			return
		}
	}
	if site.Get("region").IsFound() {
		siteRegion, lerr = site.Get("region").String()
		if lerr != nil {
			err = fmt.Errorf("error during station creation (site region): %s", lerr.Error())
			return
		}
	}
	if site.Get("country").IsFound() {
		siteCountry, lerr = site.Get("country").String()
		if lerr != nil {
			err = fmt.Errorf("error during station creation (site country): %s", lerr.Error())
			return
		}
	}
	dbirdSite := Site{
		name:        siteName,
		description: siteDescription,
		town:        siteTown,
		county:      siteCounty,
		region:      siteRegion,
		country:     siteCountry}

	channelList := station.Get("channel")
	channelSize, lerr := channelList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during station creation: %s", lerr.Error())
		return
	}
	channels := make([]dbirdv2.Channel, channelSize)
	for i := 0; i < channelSize; i++ {
		yamlChannel := channelList.GetIndex(i)
		currentChannel, lerr := NewChannel(yamlChannel)
		if lerr != nil {
			err = lerr
			return
		}
		channels[i] = currentChannel
	}
	comments, lerr := getStationComment(station.Get("comment"))
	if lerr != nil {
		err = fmt.Errorf("error getting station comments: %s", lerr.Error())
		return
	}

	startTime, lerr := timeFromString(stationDef[1])
	if lerr != nil {
		err = fmt.Errorf("error getting station start time: %s", lerr.Error())
		return
	}
	endTime, lerr := timeFromString(stationDef[2])
	if lerr != nil {
		err = fmt.Errorf("error getting station end time: %s", lerr.Error())
		return
	}
	result = &Station{
		irisCode:         stationDef[0],
		startTime:        startTime,
		endTime:          endTime,
		latitude:         latitude,
		longitude:        longitude,
		elevation:        elevation,
		waterLevel:       waterLevelP,
		site:             dbirdSite,
		observatory:      observatoryList,
		channels:         channels,
		comments:         comments,
		restrictedStatus: restrictedStatus,
		alternateCode:    alternateCode,
		historicalCode:   historicalCode,
		pzRoot:           pzRoot}
	return
}

package ydbird

import (
	"fmt"
	"io/ioutil"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// YDbird is YAML dbird implementation
type YDbird struct {
	yaml   *simpleyaml.Yaml
	pzRoot string
}

// Version is dbird interface
func (yd *YDbird) Version() float32 {
	version, _ := yd.yaml.Get("version").Float()
	return float32(version)
}

// OriginatingOrganization is dbird interface
func (yd *YDbird) OriginatingOrganization() string {
	organisation, _ := yd.yaml.Get("originating_organization").GetIndex(0).String()
	return organisation
}

// Networks is dbird interface
func (yd *YDbird) Networks() (result []dbirdv2.Network, err error) {
	networkList := yd.yaml.Get("network").Get("network")
	nbNetwork, lerr := networkList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during network creation: %s", lerr.Error())
		return
	}
	result = make([]dbirdv2.Network, nbNetwork)
	for i := 0; i < nbNetwork; i++ {
		result[i], lerr = NewNetwork(networkList.GetIndex(i))
		if lerr != nil {
			err = fmt.Errorf("error during network creation: %s", lerr.Error())
			return
		}
	}
	return
}

// Stations give the array dbirdv2.Station from YAML ydbird representation
func (yd *YDbird) Stations() (result []dbirdv2.Station, err error) {
	stationList := yd.yaml.Get("station")
	nbStation, lerr := stationList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during network creation: %s", lerr.Error())
		return
	}
	result = make([]dbirdv2.Station, nbStation)
	for i := 0; i < nbStation; i++ {
		result[i], err = NewStation(stationList.GetIndex(i), yd.pzRoot)
		if err != nil {
			return
		}
	}
	return
}

// Load load YDbird from a give filepath
func Load(ydbirdFilePath string, pzRoot string) (dbird *YDbird, err error) {
	content, lerr := ioutil.ReadFile(ydbirdFilePath)
	if lerr != nil {
		err = lerr
		return
	}
	var yaml *simpleyaml.Yaml
	yaml, err = simpleyaml.NewYaml(content)
	if err != nil {
		return
	}
	dbird = &YDbird{yaml: yaml,
		pzRoot: pzRoot}
	return
}

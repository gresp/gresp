package ydbird

import (
	"fmt"
	"time"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// Network is YAML implementation of dbird interface
type Network struct {
	fdsnCode         string
	description      string
	contactMail      string
	startTime        *time.Time
	endTime          *time.Time
	alternateCode    string
	doi              string
	restrictedStatus string
	comments         []dbirdv2.Comment
}

// FDSNCode is dbird interface
func (n Network) FDSNCode() string {
	return n.fdsnCode
}

// Description is dbird interface
func (n Network) Description() string {
	return n.description
}

// ContactMail is dbird interface
func (n Network) ContactMail() string {
	return n.contactMail
}

// StartTime is dbird interface
func (n Network) StartTime() *time.Time {
	return n.startTime
}

// EndTime is dbird interface
func (n Network) EndTime() *time.Time {
	return n.endTime
}

// AlternateCode is dbird interface
func (n Network) AlternateCode() string {
	return n.alternateCode
}

// Doi is dbird interface
func (n Network) Doi() string {
	return n.doi
}

// Comments is dbird interface
func (n Network) Comments() []dbirdv2.Comment {
	return n.comments
}

// RestrictedStatus is dbird interface
func (n Network) RestrictedStatus() string {
	return n.restrictedStatus
}

// NewNetwork create a Network from a given YAML
// [ "RA", "RESIF-RAP Accelerometric permanent network",
//   "RA@resif.fr", "1995-01-01T00:00:00Z", "present",
//   "RAP", "DOI", [*COM1] ]
func NewNetwork(network *simpleyaml.Yaml) (result Network, err error) {
	var nbComment int
	networkDef, lerr := stringSlice(network, 7)
	if lerr != nil {
		err = fmt.Errorf("error during network creation: %s", lerr.Error())
		return
	}
	commentList := network.GetIndex(7)

	nbComment, lerr = commentList.GetArraySize()
	if lerr != nil {
		err = fmt.Errorf("error during network creation: %s", lerr.Error())
		return
	}
	networkComment := make([]dbirdv2.Comment, nbComment)
	for i := 0; i < nbComment; i++ {
		currentYamlComment := commentList.GetIndex(i)
		networkComment[i], lerr = NewComment(currentYamlComment)
		if lerr != nil {
			err = fmt.Errorf("error during network creation: %s", lerr.Error())
			return
		}
	}
	restrictedStatus, lerr := network.GetIndex(8).String()
	if lerr != nil {
		err = fmt.Errorf("error getting restrictedStatus: %s", lerr.Error())
		return
	}
	startTime, lerr := timeFromString(networkDef[3])
	if lerr != nil {
		err = fmt.Errorf("error getting starttime: %s", lerr.Error())
		return
	}

	endTime, lerr := timeFromString(networkDef[4])
	if lerr != nil {
		err = fmt.Errorf("error getting endtime: %s", lerr.Error())
		return
	}

	result = Network{
		fdsnCode:         networkDef[0],
		description:      networkDef[1],
		contactMail:      networkDef[2],
		startTime:        startTime,
		endTime:          endTime,
		alternateCode:    networkDef[5],
		doi:              networkDef[6],
		restrictedStatus: restrictedStatus,
		comments:         networkComment}
	return
}

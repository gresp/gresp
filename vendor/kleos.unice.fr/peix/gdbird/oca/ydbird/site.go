package ydbird

// Site is the Ydbird representation of StationXML site
type Site struct {
	name        string
	description string
	town        string
	county      string
	region      string
	country     string
}

// Name is dbird interface
func (s Site) Name() string {
	return s.name
}

// Description is dbird interface
func (s Site) Description() string {
	return s.description
}

// Town is dbird interface
func (s Site) Town() string {
	return s.town
}

// County is dbird interface
func (s Site) County() string {
	return s.county
}

// Region is dbird interface
func (s Site) Region() string {
	return s.region
}

// Country is dbird interface
func (s Site) Country() string {
	return s.country
}

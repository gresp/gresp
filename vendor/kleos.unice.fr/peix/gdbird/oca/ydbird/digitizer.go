package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// Digitizer is digitizer abstraction
type Digitizer struct {
	dType        string
	serialNumber string
	pzFile       string
}

// Type is dbird interface
func (d Digitizer) Type() string {
	return d.dType
}

// SerialNumber is dbird interface
func (d Digitizer) SerialNumber() string {
	return d.serialNumber
}

// PZFile is dbird interface
func (d Digitizer) PZFile() string {
	return d.pzFile
}

// NewDigitizer create a new Digitizer
func NewDigitizer(digitalFilter *simpleyaml.Yaml) (result dbirdv2.Digitizer, err error) {
	filterDef, lerr := stringSlice(digitalFilter, 0)
	if lerr != nil {
		err = fmt.Errorf("error during digitizer creation: %s", lerr.Error())
		return
	}
	result = Digitizer{
		dType:        filterDef[0],
		serialNumber: filterDef[1],
		pzFile:       filterDef[2]}
	return
}

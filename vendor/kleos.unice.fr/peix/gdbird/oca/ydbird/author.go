package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// PhoneNumber is dbird interface
type PhoneNumber struct {
	countryCode *int
	phoneNumber string
	areaCode    int
}

// CountryCode is dbird interface
func (pn PhoneNumber) CountryCode() *int {
	return pn.countryCode
}

// AreaCode is dbird interface
func (pn PhoneNumber) AreaCode() int {
	return pn.areaCode
}

// PhoneNumber is dbird interface
func (pn PhoneNumber) PhoneNumber() string {
	return pn.phoneNumber
}

// Author is dbird interface
type Author struct {
	names    []string
	agencies []string
	emails   []string
	phones   []PhoneNumber
}

// Names is dbird interface
func (a Author) Names() []string {
	return a.names
}

// Agencies is dbird interface
func (a Author) Agencies() []string {
	return a.agencies
}

// Emails is dbird interface
func (a Author) Emails() []string {
	return a.emails
}

// Phones is dbird interface
func (a Author) Phones() (result []dbirdv2.PhoneNumber) {
	result = make([]dbirdv2.PhoneNumber, len(a.phones))
	for index, current := range a.phones {
		result[index] = current
	}
	return
}

// NewAuthor create a new Author from Yaml author pointer
// [ "Laboratoire Souterrain à Bas Bruit, (UMS 3538 LSBB)"], [],
//
//	["plateforme@lsbb.eu"], [] ]
func NewAuthor(author *simpleyaml.Yaml) (result Author, err error) {
	var agencies, emails []string

	names, lerr := stringSlice(author.GetIndex(0), 0)
	if lerr != nil {
		err = fmt.Errorf("error during author creation (names): %s", lerr.Error())
		return
	}
	agencies, lerr = stringSlice(author.GetIndex(1), 0)
	if lerr != nil {
		err = fmt.Errorf("error during author creation (agencies): %s", lerr.Error())
		return
	}
	emails, lerr = stringSlice(author.GetIndex(2), 0)
	if lerr != nil {
		err = fmt.Errorf("error during author creation (emails): %s", lerr.Error())
		return
	}

	result = Author{names: names, agencies: agencies, emails: emails}
	return
}

package ydbird

import (
	"fmt"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/simpleyaml"
)

// Observatory is ydbird Observatory implementation
type Observatory struct {
	contact     string
	website     string
	description string
}

// Contact is dbird interface
func (o Observatory) Contact() string {
	return o.contact
}

// WebSite is dbird interface
func (o Observatory) WebSite() string {
	return o.website
}

// Description is dbird interface
func (o Observatory) Description() string {
	return o.description
}

//
// NewObservatory create a new dbirdv2.Observatory from YAML representation.
//
func NewObservatory(observatory *simpleyaml.Yaml) (result dbirdv2.Observatory, err error) {
	if !observatory.IsArray() {
		err = fmt.Errorf("error during observatory construction: observatory must be an array")
		return
	}
	contact, lerr := observatory.GetIndex(0).String()
	if lerr != nil {
		err = fmt.Errorf("error during observatory construction: %s", lerr.Error())
		return
	}
	website, lerr := observatory.GetIndex(1).String()
	if lerr != nil {
		err = fmt.Errorf("error during observatory construction: %s", lerr.Error())
	}
	description, lerr := observatory.GetIndex(2).String()
	if lerr != nil {
		err = fmt.Errorf("error during location construction: %s", lerr.Error())
		return
	}
	result = Observatory{
		contact:     contact,
		website:     website,
		description: description}
	return
}

package dbirdv2

import (
	"time"
)

// PhoneNumber define phone number description
type PhoneNumber interface {
	CountryCode() *int
	AreaCode() int
	PhoneNumber() string
}

// Author define comment author description
type Author interface {
	Names() []string
	Agencies() []string
	Emails() []string
	Phones() []PhoneNumber
}

// Comment define comment description
type Comment interface {
	Subject() string
	Description() string
	StartTime() *time.Time
	EndTime() *time.Time
	Authors() []Author
	Type() string
}

// Network define network description
type Network interface {
	FDSNCode() string
	Description() string
	ContactMail() string
	StartTime() *time.Time
	EndTime() *time.Time
	AlternateCode() string
	RestrictedStatus() string
	Doi() string
	Comments() []Comment
}

// Observatory define observatory description
type Observatory interface {
	Contact() string
	WebSite() string
	Description() string
}

// Site define site description
type Site interface {
	Name() string
	Description() string
	Town() string
	County() string
	Region() string
	Country() string
}

// UncertaintyFloat64 define float64 value with uncertainty
type UncertaintyFloat64 interface {
	Value() float64
	PlusError() *float64
	MinusError() *float64
}

// Location define location description
type Location interface {
	Code() string
	Longitude() UncertaintyFloat64
	Latitude() UncertaintyFloat64
	Elevation() UncertaintyFloat64
	Depth() float64
	WaterLevel() UncertaintyFloat64
	Vault() string
	Geology() string
}

// Sensor define sensor description
type Sensor interface {
	Type() string
	SerialNumber() string
	PZFile() string
	Azimuth() *float64
	Dip() *float64
}

// AnalogFilter define analog filter description
type AnalogFilter interface {
	Type() string
	SerialNumber() string
	PZFile() string
}

// Digitizer define digitizer description
type Digitizer interface {
	Type() string
	SerialNumber() string
	PZFile() string
}

// DigitalFilter define digital filter description
type DigitalFilter interface {
	Type() string
	SerialNumber() string
	PZFile() string
}

// Channel define channel description
type Channel interface {
	Location() Location
	IrisCode() string
	SampleRate() float64
	Sensor() Sensor
	AnalogFilter() []AnalogFilter
	Digitizer() Digitizer
	DigitalFilter() []DigitalFilter
	Flags() string
	Encoding() string
	StartTime() *time.Time
	EndTime() *time.Time
	Network() Network
	Comments() []Comment
	RestrictedStatus() string
}

// Station define station description
type Station interface {
	IrisCode() string
	StartTime() *time.Time
	EndTime() *time.Time
	Longitude() UncertaintyFloat64
	Latitude() UncertaintyFloat64
	Elevation() UncertaintyFloat64
	WaterLevel() UncertaintyFloat64
	Observatory() []Observatory
	Site() Site
	Comments() []Comment
	Channels() []Channel
	RestrictedStatus() string
	AlternateCode() string
	HistoricalCode() string
	PzRoot() string
}

// Dbird is the type mapping dbird JSON file
type Dbird interface {
	Version() float32
	OriginatingOrganization() string
	Networks() ([]Network, error)
	Stations() ([]Station, error)
}

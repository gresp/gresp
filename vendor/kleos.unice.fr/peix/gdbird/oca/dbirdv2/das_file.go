package dbirdv2

import (
	"encoding/json"
	"io/ioutil"
)

//
// DasDescription define das description
//
type DasDescription struct {
	DbirdCode   string
	SeedCode    string
	Description string
}

//
// DasList define das_file structure
//
type DasList []*DasDescription

//
// LoadDasFile load das_list file
//
// dasFilePath the das_list file path
//
// return -- The DasList
//
func LoadDasFile(dasFilePath string, dasListSchemaPath string) (dasList *DasList, err error) {
	content, lerr := ioutil.ReadFile(dasFilePath)
	if lerr != nil {
		err = lerr
		return
	}
	schemaValidation(string(content), dasListSchemaPath)
	dasList = new(DasList)
	err = json.Unmarshal(content, dasList)
	return
}

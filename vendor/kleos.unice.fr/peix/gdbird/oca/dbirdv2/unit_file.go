package dbirdv2

import (
	"encoding/json"
	"io/ioutil"
)

//
// UnitDescription define unit description
//
type UnitDescription struct {
	DbirdCode   string
	SeedCode    string
	XMLCode     string
	Description string
}

//
// UnitList define unit_file structure
//
type UnitList []*UnitDescription

//
// LoadUnitFile load unit_list file
//
// sensorFilePath the unit_list file path
//
// return -- The UnitList
//
func LoadUnitFile(unitFilePath string, unitListSchemaPath string) (unitList *UnitList, err error) {
	content, lerr := ioutil.ReadFile(unitFilePath)
	if lerr != nil {
		err = lerr
		return
	}
	schemaValidation(string(content), unitListSchemaPath)
	unitList = new(UnitList)
	err = json.Unmarshal(content, unitList)
	return
}

package dbirdv2

import (
	"fmt"
	"path/filepath"

	"github.com/xeipuuv/gojsonschema"
)

func validate(schema *gojsonschema.Schema, loader gojsonschema.JSONLoader) (err error) {
	result, err := schema.Validate(loader)
	if err != nil {
		return
	}
	valid := result.Valid()
	if !valid {
		var errorMsg string
		for _, lerr := range result.Errors() {
			errorMsg = fmt.Sprintf("%s\n- %s", errorMsg, lerr)
		}
		err = fmt.Errorf("can't validate:\n%s", errorMsg)
	}
	return
}

func schemaValidation(content string, schemaPath string) error {
	absolutePath, err := filepath.Abs(schemaPath)
	if err != nil {
		return fmt.Errorf("filepath.Abs error: %s", err.Error())
	}
	schemaLoader := gojsonschema.NewReferenceLoader(fmt.Sprintf("file://%s", absolutePath))
	schema, err := gojsonschema.NewSchema(schemaLoader)
	if err != nil {
		return fmt.Errorf("Error during loading schema: %s", err.Error())
	}

	loader := gojsonschema.NewStringLoader(content)
	err = validate(schema, loader)
	if err != nil {
		return fmt.Errorf("Error during sensor list validation: %s", err.Error())
	}
	return nil
}

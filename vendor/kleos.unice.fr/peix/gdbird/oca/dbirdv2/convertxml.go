package dbirdv2

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"kleos.unice.fr/peix/gdbird/oca/dbirdv2/respbank"
	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

type StationXMLConverterError struct {
	network  string
	station  string
	location string
	channel  string
	severity string
	message  string
}

func (err StationXMLConverterError) Error() string {
	if err.network == "" {
		return fmt.Sprintf("%s: %s", err.severity, err.message)
	} else if err.station == "" {
		return fmt.Sprintf("%s %s: %s", err.severity, err.network, err.message)
	} else if err.channel == "" {
		return fmt.Sprintf("%s %s.%s: %s", err.severity, err.network, err.station, err.message)
	} else {
		return fmt.Sprintf("%s %s.%s.%s.%s: %s", err.severity, err.network, err.station, err.location, err.channel, err.message)
	}
}

// StationXMLConverter is a Dbird to stationXML converter
type StationXMLConverter struct {
	validator    respbank.PzValidator
	dbird        Dbird
	sensorList   *SensorList
	dasList      *DasList
	nameToUnit   map[string]stationxml.Unit
	obspyPolyBug bool
}

// createUnitTable initialize association between unit dbird code with stationxml.Unit
func createUnitTable(unitList *UnitList) map[string]stationxml.Unit {
	nameToUnit := make(map[string]stationxml.Unit)
	for _, currentUnit := range *unitList {
		xmlUnit := stationxml.Unit{
			Name:        currentUnit.XMLCode,
			Description: currentUnit.Description}
		nameToUnit[currentUnit.DbirdCode] = xmlUnit
	}
	return nameToUnit
}

// NewStationXMLConverter create a new dbird to stationXML converter
func NewStationXMLConverter(sensorList *SensorList, dasList *DasList,
	unitList *UnitList, schemaRootDir string, obspyPolyBug bool) (result *StationXMLConverter, err error) {
	var validator respbank.PzValidator
	validator, err = respbank.NewPzValidator(schemaRootDir)
	if err != nil {
		return
	}
	result = &StationXMLConverter{
		sensorList:   sensorList,
		dasList:      dasList,
		nameToUnit:   createUnitTable(unitList),
		validator:    validator,
		obspyPolyBug: obspyPolyBug}

	return
}

// Seed2XMLFlags is a mapping between Seed channel flags and station XML channel flags
var Seed2XMLFlags = map[rune]string{'T': "TRIGGERED", 'C': "CONTINUOUS",
	'H': "HEALTH", 'G': "GEOPHYSICAL", 'W': "WEATHER", 'F': "FLAG",
	'S': "SYNTHESIZED", 'I': "INPUT", 'E': "EXPERIMENTAL", 'M': "MAINTENANCE",
	'B': "BEAM"}

func createFloat(value UncertaintyFloat64, unit string) stationxml.Float {
	return stationxml.MakeFloat(value.Value(), value.MinusError(),
		value.PlusError(), nil, unit)
}

func networkStationsList(stationsList []Station) map[string][]Station {
	network2Stations := make(map[string][]Station)
	// For each dbird station
	for _, currentStation := range stationsList {
		// For each channel of currentStation
		for _, currentChannel := range currentStation.Channels() {
			var stations []Station
			var present = false
			// Test if currentStation is already associated with currentChannel.Network
			stations = network2Stations[currentChannel.Network().FDSNCode()]
			for _, current := range stations {
				if current == currentStation {
					present = true
				}
			}
			if !present {
				stations = append(stations, currentStation)
				network2Stations[currentChannel.Network().FDSNCode()] = stations
			}
		}
	}
	return network2Stations
}

func (c *StationXMLConverter) createAuthor(author Author) (result stationxml.Person) {
	result = stationxml.Person{}
	result.Name = append(result.Name, author.Names()...)
	result.Agency = append(result.Agency, author.Agencies()...)
	result.Email = append(result.Email, author.Emails()...)
	for _, currentPhone := range author.Phones() {
		xmlPhone := stationxml.Phone{
			CountryCode: currentPhone.CountryCode(),
			AreaCode:    currentPhone.AreaCode(),
			PhoneNumber: currentPhone.PhoneNumber()}
		result.Phone = append(result.Phone, xmlPhone)
	}
	return
}

// createComment create stationxml.comment from dbird.Comment
func (c *StationXMLConverter) createComment(comment Comment) (result stationxml.Comment, err error) {
	if comment.StartTime() == nil {
		err = fmt.Errorf("start time not define for comment %s", comment.Subject())
		return
	}

	var endTime string
	if comment.EndTime() != nil {
		endTime = comment.EndTime().Format(stationxml.DateFormat)
	}
	var authorList []stationxml.Person
	for _, current := range comment.Authors() {
		authorList = append(authorList, c.createAuthor(current))
	}
	result = stationxml.Comment{
		Subject:            comment.Subject(),
		Value:              comment.Description(),
		BeginEffectiveTime: comment.StartTime().Format(stationxml.DateFormat),
		EndEffectiveTime:   endTime,
		Author:             authorList}
	return
}

// createSite create stationxmlSite from dbird.Site
func (c *StationXMLConverter) createSite(dbirdSite Site) stationxml.Site {
	return stationxml.Site{
		Name:        dbirdSite.Name(),
		Description: dbirdSite.Description(),
		Town:        dbirdSite.Town(),
		County:      dbirdSite.County(),
		Region:      dbirdSite.Region(),
		Country:     dbirdSite.Country()}
}

func (c *StationXMLConverter) createSensorEquipement(dbirdSensor Sensor) (result *stationxml.Equipement, err error) {
	var sensorDesc *SensorDescription
	for _, current := range *c.sensorList {
		if current.DbirdCode == dbirdSensor.Type() {
			sensorDesc = current
			break
		}
	}
	if sensorDesc == nil {
		err = fmt.Errorf("can't find sensor with code %s", dbirdSensor.Type())
		return
	}
	result = &stationxml.Equipement{
		Description:  sensorDesc.Description,
		Model:        sensorDesc.SeedCode,
		SerialNumber: dbirdSensor.SerialNumber()}

	return
}

func (c *StationXMLConverter) createDigitizerEquipement(dbirdDigitizer Digitizer) (result *stationxml.Equipement, err error) {
	var sensorDesc *DasDescription
	for _, current := range *c.dasList {
		if current.DbirdCode == dbirdDigitizer.Type() {
			sensorDesc = current
			break
		}
	}
	if sensorDesc == nil {
		err = fmt.Errorf("can't find digitizer with code %s", dbirdDigitizer.Type())
		return
	}
	result = &stationxml.Equipement{
		Description:  sensorDesc.Description,
		Model:        sensorDesc.SeedCode,
		SerialNumber: dbirdDigitizer.SerialNumber()}

	return
}

// CreateXMLStation create stationxml.Station from dbird.Station
func (c *StationXMLConverter) createXMLStation(station Station,
	fdsnCode string) (result stationxml.Station, err []StationXMLConverterError) {

	if station.StartTime() == nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			message:  "Start time is not defined"})
		return
	}
	var endTime string
	if station.EndTime() != nil {
		endTime = station.EndTime().Format(stationxml.DateFormat)
	}
	var OperatorList []stationxml.Operator
	for _, observatory := range station.Observatory() {
		var operator *stationxml.Person
		if observatory.Contact() != "" {
			operator = &stationxml.Person{Email: []string{observatory.Contact()}}
		}
		OperatorList = append(OperatorList, stationxml.Operator{
			Agency:  observatory.Description(),
			Contact: operator,
			WebSite: observatory.WebSite()})
	}
	sort.Slice(OperatorList, func(i int, j int) bool { return OperatorList[i].Agency < OperatorList[j].Agency })
	var waterLevelP *stationxml.Float
	if station.WaterLevel() != nil {
		waterLevel := createFloat(station.WaterLevel(), "m")
		waterLevelP = &waterLevel
	}
	result = stationxml.Station{
		BaseNode: stationxml.BaseNode{
			Code:             station.IrisCode(),
			StartDate:        station.StartTime().Format(stationxml.DateFormat),
			EndDate:          endTime,
			RestrictedStatus: station.RestrictedStatus(),
			AlternateCode:    station.AlternateCode(),
			HistoricalCode:   station.HistoricalCode()},
		Latitude:               createFloat(station.Latitude(), ""),
		Longitude:              createFloat(station.Longitude(), ""),
		Elevation:              createFloat(station.Elevation(), ""),
		WaterLevel:             waterLevelP,
		Site:                   c.createSite(station.Site()),
		Operator:               OperatorList,
		CreationDate:           station.StartTime().Format(stationxml.DateFormat),
		TerminationDate:        endTime,
		TotalNumberChannels:    len(station.Channels()),
		SelectedNumberChannels: len(station.Channels())}

	for _, currentComment := range station.Comments() {
		if currentComment.Type() == "S" {
			xmlComment, cerr := c.createComment(currentComment)
			if cerr != nil {
				err = append(err, StationXMLConverterError{
					severity: "Warning",
					network:  fdsnCode,
					station:  station.IrisCode(),
					message:  cerr.Error()})
				continue
			}
			result.Comment = append(result.Comment, xmlComment)
		}
	}
	for _, current := range station.Channels() {
		if current.Network().FDSNCode() == fdsnCode {
			newChannel, cerr := c.createXMLChannel(fdsnCode, station, current)
			if cerr != nil {
				err = append(err, cerr...)
				continue
			}
			result.Channel = append(result.Channel, newChannel)
		}
	}
	return
}

// CreateXMLChannel create stationxmlChannel from dbird.Channel
func (c *StationXMLConverter) createXMLChannel(fdsnCode string, station Station, channel Channel) (result stationxml.Channel, err []StationXMLConverterError) {

	sensor := channel.Sensor()
	var dip, azimuth *stationxml.Float
	if sensor.Dip() != nil {
		tmp := stationxml.MakeFloat(*sensor.Dip(), nil, nil, nil, "")
		dip = &tmp
	}
	if sensor.Azimuth() != nil {
		tmp := stationxml.MakeFloat(*sensor.Azimuth(), nil, nil, nil, "")
		azimuth = &tmp
	}
	sampleRate := stationxml.MakeFloat(channel.SampleRate(), nil, nil, nil, "")
	location := channel.Location()
	depth := stationxml.MakeFloat(location.Depth(), nil, nil, nil, "")
	var endTime string
	if channel.EndTime() != nil {
		endTime = channel.EndTime().Format(stationxml.DateFormat)
	}

	stagedSensor, sensorErr := respbank.LoadSensor(station.PzRoot(), sensor.PZFile(),
		c.getUnit, c.validator)
	if sensorErr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			location: location.Code(),
			channel:  channel.IrisCode(),
			message:  sensorErr.Error()})
		return
	}
	isPolynomial := false
	// Determine if sensor response is Polynomial
	switch stagedSensor.(type) {
	case *respbank.PolynomialSensor:
		isPolynomial = true
	}
	// Fix TransferNormalizationFrequency according to channel sample rate
	switch s := stagedSensor.(type) {
	case *respbank.PZSensor:
		s.FixTransferNormalizationFrequency(channel.SampleRate())
	}

	sensorEquipement, sensorEquipErr := c.createSensorEquipement(sensor)
	if sensorEquipErr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			location: location.Code(),
			channel:  channel.IrisCode(),
			message:  sensorEquipErr.Error()})
		return
	}

	response, rerr := createXMLResponse(stagedSensor)
	if rerr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			location: location.Code(),
			channel:  channel.IrisCode(),
			message:  rerr.Error()})
		return
	}

	//
	// Fill channel response
	//
	currentStageNumber := 1

	// add sensor response
	sensorStages, sensorStageErr := stagedSensor.XMLStage(currentStageNumber)
	if sensorStageErr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			location: location.Code(),
			channel:  channel.IrisCode(),
			message:  sensorStageErr.Error()})
		return
	}
	if !isPolynomial || !c.obspyPolyBug {
		// In case of obspyPolyBug is set response contains only global response
		response.Stage = append(response.Stage, sensorStages...)
	}
	currentStageNumber += len(sensorStages)

	// add analog filter response
	for _, current := range channel.AnalogFilter() {
		currentAnalogFilter := current
		stagedAnalogFilter, stagedAnalogFilterErr := respbank.LoadAnalogFilter(station.PzRoot(),
			currentAnalogFilter.PZFile(), c.getUnit, c.validator)
		if stagedAnalogFilterErr != nil {
			err = append(err, StationXMLConverterError{
				severity: "Error",
				network:  fdsnCode,
				station:  station.IrisCode(),
				location: location.Code(),
				channel:  channel.IrisCode(),
				message:  stagedAnalogFilterErr.Error()})
			return
		}
		analogFilterStages, analogFilterStagesErr := stagedAnalogFilter.XMLStage(currentStageNumber)
		if analogFilterStagesErr != nil {
			err = append(err, StationXMLConverterError{
				severity: "Error",
				network:  fdsnCode,
				station:  station.IrisCode(),
				location: location.Code(),
				channel:  channel.IrisCode(),
				message:  analogFilterStagesErr.Error()})
			return
		}
		if !isPolynomial || !c.obspyPolyBug {
			// In case of obspyPolyBug is set response contains only global response
			response.Stage = append(response.Stage, analogFilterStages...)
		}
		currentStageNumber += len(analogFilterStages)

		// Update full channel response with current sensitivity
		response.UpdateSensitivity(stagedAnalogFilter.Sensitivity())
	}
	// add digitizer response

	currentDigitizer := channel.Digitizer()

	digitizerEquipement, digitizerEquipErr := c.createDigitizerEquipement(currentDigitizer)
	if digitizerEquipErr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			location: location.Code(),
			channel:  channel.IrisCode(),
			message:  digitizerEquipErr.Error()})
		return
	}

	stagedDigitizer, stagedDigitizerErr := respbank.LoadDigitizer(station.PzRoot(),
		currentDigitizer.PZFile(), c.getUnit, c.validator)
	if stagedDigitizerErr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			location: location.Code(),
			channel:  channel.IrisCode(),
			message:  stagedDigitizerErr.Error()})
		return
	}
	digitizerStages, digitizerStagesErr := stagedDigitizer.XMLStage(currentStageNumber)
	if digitizerStagesErr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			network:  fdsnCode,
			station:  station.IrisCode(),
			location: location.Code(),
			channel:  channel.IrisCode(),
			message:  digitizerStagesErr.Error()})
		return
	}
	if !isPolynomial || !c.obspyPolyBug {
		// In case of obspyPolyBug is set response contains only global response
		response.Stage = append(response.Stage, digitizerStages...)
	}
	currentStageNumber += len(digitizerStages)
	// Update full channel response with current sensitivity
	response.UpdateSensitivity(stagedDigitizer.Sensitivity())

	// add digital filter response
	for _, current := range channel.DigitalFilter() {
		currentDigitalFilter := current
		stagedDigitalFilters, stagedDigitalFiltersErr := respbank.LoadDigitalFilter(station.PzRoot(),
			currentDigitalFilter.PZFile(), c.getUnit, c.validator)
		if stagedDigitalFiltersErr != nil {
			err = append(err, StationXMLConverterError{
				severity: "Error",
				network:  fdsnCode,
				station:  station.IrisCode(),
				location: location.Code(),
				channel:  channel.IrisCode(),
				message:  stagedDigitalFiltersErr.Error()})
			return
		}
		for _, stagedDigitalFilter := range stagedDigitalFilters {
			digitalFilterStages, digitalFilterStagesErr := stagedDigitalFilter.XMLStage(currentStageNumber)
			if digitalFilterStagesErr != nil {
				err = append(err, StationXMLConverterError{
					severity: "Error",
					network:  fdsnCode,
					station:  station.IrisCode(),
					location: location.Code(),
					channel:  channel.IrisCode(),
					message:  digitalFilterStagesErr.Error()})
				return
			}
			if !isPolynomial || !c.obspyPolyBug {
				// In case of obspyPolyBug is set response contains only global response
				response.Stage = append(response.Stage, digitalFilterStages...)
			}
			currentStageNumber += len(digitalFilterStages)
			// Update full channel response with current sensitivity
			response.UpdateSensitivity(stagedDigitalFilter.Sensitivity())
		}
	}
	var waterLevelP *stationxml.Float
	if location.WaterLevel() != nil {
		waterLevel := createFloat(location.WaterLevel(), "m")
		waterLevelP = &waterLevel
	}
	result = stationxml.Channel{
		BaseNode: stationxml.BaseNode{
			StartDate:        channel.StartTime().Format(stationxml.DateFormat),
			EndDate:          endTime,
			Code:             channel.IrisCode(),
			RestrictedStatus: channel.RestrictedStatus()},
		LocationCode: location.Code(),
		Latitude:     createFloat(location.Latitude(), ""),
		Longitude:    createFloat(location.Longitude(), ""),
		Elevation:    createFloat(location.Elevation(), ""),
		Depth:        depth,
		WaterLevel:   waterLevelP,
		Azimuth:      azimuth,
		Dip:          dip,
		SampleRate:   &sampleRate,
		Type:         result.Type,
		Sensor:       sensorEquipement,
		DataLogger:   digitizerEquipement,
		Response:     &response}

	// Convert Seed flags to XML flags
	for _, current := range channel.Flags() {
		result.Type = append(result.Type, Seed2XMLFlags[current])
	}

	for _, currentComment := range channel.Comments() {
		if currentComment.Type() == "C" {
			xmlCommentLabel, cerr := c.createComment(currentComment)
			if cerr != nil {
				err = append(err, StationXMLConverterError{
					severity: "Warning",
					network:  fdsnCode,
					station:  station.IrisCode(),
					location: location.Code(),
					channel:  channel.IrisCode(),
					message:  cerr.Error()})
				continue
			}
			result.Comment = append(result.Comment, xmlCommentLabel)
		}
	}
	// Reset TransferNormalizationFrequency according to channel sample rate
	switch s := stagedSensor.(type) {
	case *respbank.PZSensor:
		s.ResetTransferNormalizationFrequency()
	}

	return
}

// CreateXMLResponse create stationxmlResponse
func createXMLResponse(stagedSensor respbank.Staged) (result stationxml.Response, err error) {

	if pzSensor, ok := stagedSensor.(*respbank.PZSensor); ok {
		result, err = pzSensor.Response()
	} else if polySensor, ok := stagedSensor.(*respbank.PolynomialSensor); ok {
		result, err = polySensor.Response()
	} else {
		err = fmt.Errorf("unknown sensor type")
	}

	return
}

func (c *StationXMLConverter) getUnit(dbirdCode string) (result stationxml.Unit, err error) {
	var ok bool
	result, ok = c.nameToUnit[strings.ToUpper(dbirdCode)]
	if !ok {
		err = fmt.Errorf("unknown dbird unit code (%s)", dbirdCode)
	}
	return
}

// Convert convert given Dbird to FDSNStationXML
func (c *StationXMLConverter) Convert(dbirdFile Dbird) (result stationxml.FDSNStationXML, err []StationXMLConverterError) {
	// Create base
	c.dbird = dbirdFile
	stations, lerr := dbirdFile.Stations()
	if lerr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			message:  fmt.Sprintf("Can't decode dbird file: %s", lerr.Error())})
		return
	}
	network2Stations := networkStationsList(stations)
	now := time.Now().UTC().Truncate(time.Second)
	result = stationxml.FDSNStationXML{
		Xsi:           "http://www.w3.org/2001/XMLSchema-instance",
		Xmlns:         "http://www.fdsn.org/xml/station/1",
		SchemaVersion: "1.1",
		Source:        dbirdFile.OriginatingOrganization(),
		Sender:        "RESIF",
		Module:        "Gresp",
		Created:       now.Format(stationxml.DateFormat)}

	networkList, lerr := dbirdFile.Networks()
	if lerr != nil {
		err = append(err, StationXMLConverterError{
			severity: "Error",
			message:  fmt.Sprintf("Can't get network list: %s", lerr.Error())})
		return
	}
	for _, currentNetwork := range networkList {
		var xmlNet stationxml.Network
		var endTime string
		var doi *stationxml.Identifier
		if currentNetwork.StartTime() == nil {
			err = append(err, StationXMLConverterError{
				severity: "Error",
				network:  currentNetwork.FDSNCode(),
				message:  "Start time is not defined"})
			continue
		}
		if currentNetwork.EndTime() != nil {
			endTime = currentNetwork.EndTime().Format(stationxml.DateFormat)
		}
		if currentNetwork.Doi() != "" {
			tmp := stationxml.Identifier{
				Type: "DOI",
				Name: currentNetwork.Doi()}
			doi = &tmp
		}
		xmlNet = stationxml.Network{
			BaseNode: stationxml.BaseNode{
				Code:             currentNetwork.FDSNCode(),
				StartDate:        currentNetwork.StartTime().Format(stationxml.DateFormat),
				EndDate:          endTime,
				Identifier:       doi,
				RestrictedStatus: currentNetwork.RestrictedStatus(),
				Description:      currentNetwork.Description()}}
		for _, currentComment := range currentNetwork.Comments() {
			xmlComment, cerr := c.createComment(currentComment)
			if cerr != nil {
				err = append(err, StationXMLConverterError{
					severity: "Warning",
					network:  currentNetwork.FDSNCode(),
					message:  cerr.Error()})
				continue
			}
			xmlNet.Comment = append(xmlNet.Comment, xmlComment)
		}
		for _, currentStation := range network2Stations[currentNetwork.FDSNCode()] {
			newStation, stationError := c.createXMLStation(currentStation, currentNetwork.FDSNCode())
			if len(stationError) != 0 {
				err = append(err, stationError...)
				continue
			}
			xmlNet.Station = append(xmlNet.Station, newStation)
		}
		result.Network = append(result.Network, xmlNet)
	}
	return
}

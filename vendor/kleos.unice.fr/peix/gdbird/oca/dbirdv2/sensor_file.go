package dbirdv2

import (
	"encoding/json"
	"io/ioutil"
)

//
// SensorDescription define sensor description
//
type SensorDescription struct {
	DbirdCode   string
	SeedCode    string
	Type        string
	Band        string
	Description string
}

//
// SensorList define sensor_file structure
//
type SensorList []*SensorDescription

//
// LoadSensorFile load sensor_list file
//
// sensorFilePath the sensor_list file path
//
// return -- The SensorList
//
func LoadSensorFile(sensorFilePath string, sensorListSchemaPath string) (sensorList *SensorList, err error) {
	content, lerr := ioutil.ReadFile(sensorFilePath)
	if lerr != nil {
		err = lerr
		return
	}
	schemaValidation(string(content), sensorListSchemaPath)
	sensorList = new(SensorList)
	err = json.Unmarshal(content, sensorList)
	return
}

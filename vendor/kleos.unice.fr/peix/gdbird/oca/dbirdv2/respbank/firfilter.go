package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"

	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// FIRFilter define FIR digital filtrer response
//
type FIRFilter struct {
	AbstractDigitalFilter
	FIRResponse
}

//
// FIRResponse define define specific part of fir filter response
//
type FIRResponse struct {
	Name         *string
	Coefficients []float64
}

//
// FIRFilterInclude define include content of fir filter
//
type FIRFilterInclude struct {
	Coefficients []float64
}

func (f *FIRFilter) loadInclude(respRoot string) error {
	var err error
	if f.Include == nil {
		return nil
	}

	content, lerr := ioutil.ReadFile(path.Join(respRoot, *f.Include))
	if lerr != nil {
		return lerr
	}
	include := new(FIRFilterInclude)
	err = json.Unmarshal(content, include)
	if err != nil {
		return err
	}
	if include.Coefficients != nil {
		f.Coefficients = include.Coefficients
	}
	return nil
}

func (f *FIRFilter) coefficients() []float64 {
	if f.TransferFunctionType == FIRSYMODD {
		return f.Coefficients[:len(f.Coefficients)/2+1]
	} else if f.TransferFunctionType == FIRSYMEVEN {
		return f.Coefficients[:len(f.Coefficients)/2]
	}
	return f.Coefficients
}

//
// XMLStage give the XML stages associated with this PZSensor
//
func (f *FIRFilter) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	inputUnit, erri := f.getXMLUnit("COUNTS")
	if erri != nil {
		err = fmt.Errorf("Error converting %s: %s", "COUNTS", erri.Error())
		return
	}
	outputUnit, erro := f.getXMLUnit("COUNTS")
	if erro != nil {
		err = fmt.Errorf("Error converting %s: %s", "COUNTS", erro.Error())
		return
	}
	var symmetry string
	if f.TransferFunctionType == FIRASYM {
		symmetry = "NONE"
	} else if f.TransferFunctionType == FIRSYMODD {
		symmetry = "ODD"
	} else if f.TransferFunctionType == FIRSYMEVEN {
		symmetry = "EVEN"
	} else {
		err = fmt.Errorf("%s is an unknown symmetry value for FIR filter",
			f.TransferFunctionType)
		return
	}
	fir := stationxml.Fir{
		BaseFilter: stationxml.BaseFilter{
			InputUnit:  inputUnit,
			OutputUnit: outputUnit},
		Symmetry: symmetry}
	for index, current := range f.coefficients() {
		currentCoef := struct {
			Value float64 `xml:",chardata"`
			Index int     `xml:"i,attr"`
		}{Value: current, Index: index}
		fir.NumeratorCoefficient = append(fir.NumeratorCoefficient, currentCoef)
	}
	decimation := f.decimation()
	gain := f.gain()
	stage := stationxml.Stage{Number: nextStageIndex, FIR: &fir,
		Decimation: &decimation, StageGain: &gain}
	result = append(result, stage)
	return
}

func loadFIRFilter(respRoot string, filterFilePath string, abstract AbstractDigitalFilter, jsonStr []byte) (result *FIRFilter, err error) {
	firResponse := new(FIRResponse)
	lerr := json.Unmarshal(jsonStr, firResponse)
	if lerr != nil {
		err = fmt.Errorf("error decoding %s: %s", filterFilePath, lerr.Error())
		return
	}
	result = &FIRFilter{abstract, *firResponse}
	lerr = result.loadInclude(respRoot)
	if lerr != nil {
		err = fmt.Errorf("error when loading include %s from %s: %s",
			*result.Include, filterFilePath, lerr.Error())
		return
	}
	lerr = result.isValid()
	if lerr != nil {
		err = fmt.Errorf("error in %s: %s", filterFilePath, lerr.Error())
		return
	}
	return
}

//
// Sensitivity give the sensitivity of this filter
//
func (f *FIRFilter) Sensitivity() float64 {
	return *f.AbstractDigitalFilter.Sensitivity
}

package respbank

import (
	"fmt"
	"math"

	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

const (
	// ZEROES define zeroes root type
	ZEROES = "ZEROES"
	// POLES define poles root type
	POLES = "POLES"
	// ANALOG define pz response in Hz
	ANALOG = "ANALOG"
	// LAPLACE define  pz response in rad/s
	LAPLACE = "LAPLACE"
	// POLYNOMIAL define polynomial response
	POLYNOMIAL = "POLYNOMIAL"
	// IIRPZ define iir wth poles/zeroes digital filter
	IIRPZ = "IIR_PZ"
	// IIRCOEF define iir wth coefficients digital filter
	IIRCOEF = "IIR_COEF"
	// FIRSYMODD define odd fir filter
	FIRSYMODD = "FIR_SYM_1"
	// FIRSYMEVEN define even fir filter
	FIRSYMEVEN = "FIR_SYM_2"
	// FIRASYM define asymetric fir filter
	FIRASYM = "FIR_ASYM"
	// EPSILON define the minimal value to be considered as different from zero
	EPSILON = 1.e-8
)

//
// Staged define object that can generate SEED Stage and XML stage
//
type Staged interface {
	XMLStage(nextStageIndex int) ([]stationxml.Stage, error)
	Sensitivity() float64
}

//
// CreatePoleZero create a stationxml.PoleZero from a complex
//
func CreatePoleZero(index int, val complex128, valError *complex128) stationxml.PoleZero {
	var realPart, imagPart stationxml.FloatNoUnit
	if valError == nil {
		realPart = stationxml.FloatNoUnit{Value: real(val)}
		imagPart = stationxml.FloatNoUnit{Value: imag(val)}
	} else {
		realError := real(*valError)
		imagError := imag(*valError)
		realPart = stationxml.FloatNoUnit{
			Value:    real(val),
			MinError: &realError,
			MaxError: &realError}
		imagPart = stationxml.FloatNoUnit{
			Value:    imag(val),
			MinError: &imagError,
			MaxError: &imagError}
	}
	return stationxml.PoleZero{
		Number:    index,
		Real:      realPart,
		Imaginary: imagPart}
}

//
// checkRoots make verifications on given poles/zeros.
//
//   -- roots      The ploes/zeros slice
//   -- filterType The type of filter (Ex: IIRPZ, ANALOG, ...)
//   -- PZType     POLES/ZEROES
//
func checkRoots(roots []complex128, filterType string, PZType string, damped bool) error {

	if PZType == POLES && damped {
		for _, current := range roots {
			if filterType != IIRPZ && real(current) > 0.0 {
				return fmt.Errorf("checkRoots: %s real part not negative", PZType)
			}
		}
	}
	for i := 0; i < len(roots); {
		if math.Abs(imag(roots[i])) < EPSILON {
			i++
			continue
		}
		if i+1 >= len(roots) {
			return fmt.Errorf("can't find conjugate for %v", roots[i])
		}
		if real(roots[i]) != real(roots[i+1]) {
			diff := math.Abs((real(roots[i]) - real(roots[i+1])))
			if diff > EPSILON {
				return fmt.Errorf("check_roots: %s real parts not equal %.10e %.10e\nerr = %.7e",
					PZType, real(roots[i]), real(roots[i+1]), diff)
			}
		}
		if imag(roots[i])+imag(roots[i+1]) != 0. {
			diff := math.Abs((imag(roots[i]) + imag(roots[i+1])))
			if diff > EPSILON {
				return fmt.Errorf("check_roots: %s imag parts not opposite %.10e %.10e\nerr = %.7e",
					PZType, imag(roots[i]), imag(roots[i+1]), diff)
			}
		}
		i += 2
	}
	return nil
}

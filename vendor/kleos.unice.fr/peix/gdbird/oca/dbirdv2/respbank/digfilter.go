package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"strings"

	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// JSONDigitalFilterCascade define cascading of digital filter
// (The content of response databank files)
//
type JSONDigitalFilterCascade []JSONDigitalFilter

//
// AbstractDigitalFilter define common digital filtrer response
//
type AbstractDigitalFilter struct {
	TransferFunctionType string
	InputSamplingRate    *float64
	DecimationFactor     *int64
	DecimationOffset     *int64
	FrequencyOfGain      *float64
	Sensitivity          *float64
	Delay                *float64
	Correction           *float64
	Fake                 *bool
	Include              *string
	getXMLUnit           func(label string) (stationxml.Unit, error)
}

//
// JSONDigitalFilter define type used to load json file from response databank
//
type JSONDigitalFilter struct {
	AbstractDigitalFilter
	Response *json.RawMessage
}

//
// inputSampleRate give the input sample rate of this digital filter
//
func (f *AbstractDigitalFilter) inputSampleRate() float64 {
	return *f.InputSamplingRate
}

//
// decimation create stationxml.Decimation for this AbstractDigitalFilter
//
func (f *AbstractDigitalFilter) decimation() (result stationxml.Decimation) {
	var decimationOffset int64
	if f.DecimationOffset != nil {
		decimationOffset = *f.DecimationOffset
	}
	delay := 0.0
	if f.Delay != nil {
		delay = *f.Delay
	}
	correction := 0.0
	if f.Correction != nil {
		correction = *f.Correction
	}

	result = stationxml.Decimation{
		InputSampleRate: stationxml.MakeFloat(f.inputSampleRate(), nil, nil, nil, "HERTZ"),
		Factor:          int(*f.DecimationFactor),
		Offset:          int(decimationOffset),
		Delay:           stationxml.MakeFloat(delay, nil, nil, nil, ""),
		Correction:      stationxml.MakeFloat(correction, nil, nil, nil, "")}
	return
}

//
// gain give the stationxml.Gain for this AbstractDigitalFilter
//
func (f *AbstractDigitalFilter) gain() (result stationxml.Gain) {
	frequencyOfGain := 0.0
	if f.FrequencyOfGain != nil {
		frequencyOfGain = *f.FrequencyOfGain
	}
	result = stationxml.Gain{Value: *f.Sensitivity,
		Frequency: frequencyOfGain}
	return
}

func (f *AbstractDigitalFilter) isValid() error {
	if f.Fake != nil && *f.Fake {
		return nil
	}
	if f.InputSamplingRate == nil {
		return fmt.Errorf("InputSamplingRate must be define")
	}
	if f.DecimationFactor == nil {
		return fmt.Errorf("DecimationFactor must be define")
	}
	if f.DecimationOffset != nil {
		if *f.DecimationOffset < 0 {
			return fmt.Errorf("DecimationOffset must be > 0")
		}
		if *f.DecimationOffset >= *f.DecimationFactor {
			return fmt.Errorf("DecimationOffset must be < DecimationFactor")
		}
	}
	if f.Sensitivity == nil {
		return fmt.Errorf("Sensitivity must be define")
	}
	return nil
}

//
// XMLStage is a method returning no stage is used for fake digital filter
//
func (f *AbstractDigitalFilter) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	return
}

//
// Sensitivity is a fake method  fake digital filter
//
func (f *JSONDigitalFilter) Sensitivity() (result float64) {
	return 1
}

//
// LoadDigitalFilter load digital filter cascade response from response databank
//
// respRoot       -- The response databank root directory
// filterFilePath -- The file path of filter cascade response
// getUnit        -- Function to retreive abbreviation.Unit from unit code
// getXMLUnit     -- Function to retreive abbreviation.Unit from unit code
// return         -- The slice of staged digitial filter
//
func LoadDigitalFilter(respRoot string, filterFilePath string,
	getXMLUnit func(label string) (stationxml.Unit, error),
	validator PzValidator) (result []Staged, err error) {
	content, lerr := ioutil.ReadFile(path.Join(respRoot, filterFilePath))
	if lerr != nil {
		err = fmt.Errorf("Error reading %s: %s", filterFilePath, lerr.Error())
		return
	}
	err = validator.DigitalFilterValidation(string(content))
	if err != nil {
		return
	}
	cascade := new(JSONDigitalFilterCascade)
	lerr = json.Unmarshal(content, cascade)
	if lerr != nil {
		err = fmt.Errorf("Error decoding %s: %s", filterFilePath, lerr.Error())
		return
	}
	for _, currentFilter := range *cascade {
		lerr = currentFilter.isValid()
		if lerr != nil {
			err = fmt.Errorf("%s in %s", lerr.Error(), filterFilePath)
			return
		}
		if currentFilter.Fake != nil && *currentFilter.Fake {
			result = append(result, &currentFilter)
		} else if strings.Compare(currentFilter.TransferFunctionType, FIRASYM) == 0 ||
			strings.Compare(currentFilter.TransferFunctionType, FIRSYMODD) == 0 ||
			strings.Compare(currentFilter.TransferFunctionType, FIRSYMEVEN) == 0 {
			firFilter, firErr := loadFIRFilter(respRoot, filterFilePath, currentFilter.AbstractDigitalFilter,
				*currentFilter.Response)
			if firErr != nil {
				err = fmt.Errorf("Error loading FIR filter: %s", firErr.Error())
				return
			}
			firFilter.getXMLUnit = getXMLUnit
			result = append(result, firFilter)
		} else if strings.Compare(currentFilter.TransferFunctionType, IIRPZ) == 0 {
			iirPzFilter, iirErr := loadIIRPZFilter(respRoot, filterFilePath, currentFilter.AbstractDigitalFilter,
				*currentFilter.Response)
			if iirErr != nil {
				err = fmt.Errorf("Error loading IIR PZ filter: %s", iirErr.Error())
				return
			}
			iirPzFilter.getXMLUnit = getXMLUnit
			result = append(result, iirPzFilter)
		} else if strings.Compare(currentFilter.TransferFunctionType, IIRCOEF) == 0 {
			iirCoefFilter, iirErr := loadIIRCoefFilter(respRoot, filterFilePath, currentFilter.AbstractDigitalFilter,
				*currentFilter.Response)
			if iirErr != nil {
				err = fmt.Errorf("Error loading IIR Coefficient filter: %s", iirErr.Error())
				return
			}
			iirCoefFilter.getXMLUnit = getXMLUnit
			result = append(result, iirCoefFilter)
		} else {
			err = fmt.Errorf("Unknown transfer function type %s in %s",
				currentFilter.TransferFunctionType, filterFilePath)
			return
		}
	}
	return
}

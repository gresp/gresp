package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"

	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// Digitizer define digitizer response
//
type Digitizer struct {
	InputUnit          *string
	OutputUnit         *string
	OutputSamplingRate *float64
	SensitivityF       *float64 `json:"Sensitivity"`
	getXMLUnit         func(label string) (stationxml.Unit, error)
}

func (d *Digitizer) isValid() error {
	if d.InputUnit == nil {
		return fmt.Errorf("missing InputUnit")
	}
	if d.OutputUnit == nil {
		return fmt.Errorf("missing OutputUnit")
	}
	if d.OutputSamplingRate == nil {
		return fmt.Errorf("missing OutputSamplingRate")
	}
	if d.SensitivityF == nil {
		return fmt.Errorf("missing Sensitivity")
	}
	return nil
}

//
// Sensitivity give the sensitivity of this Digitizer
//
func (d *Digitizer) Sensitivity() float64 {
	return *d.SensitivityF
}

//
// XMLStage give the slice of XML stage associated with this Digitizer
//
func (d *Digitizer) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	inputUnit, erri := d.getXMLUnit(*d.InputUnit)
	if erri != nil {
		err = fmt.Errorf("Error converting %s: %s", *d.InputUnit, erri.Error())
		return
	}
	outputUnit, erro := d.getXMLUnit(*d.OutputUnit)
	if erro != nil {
		err = fmt.Errorf("Error converting %s: %s", *d.OutputUnit, erro.Error())
		return
	}

	dig := stationxml.Decimation{
		InputSampleRate: stationxml.Float{
			FloatNoUnit: stationxml.FloatNoUnit{
				Value: *d.OutputSamplingRate}},
		Factor: 1,
		Offset: 0,
		Delay: stationxml.Float{
			FloatNoUnit: stationxml.FloatNoUnit{
				Value: 0}},
		Correction: stationxml.Float{
			FloatNoUnit: stationxml.FloatNoUnit{
				Value: 0}}}

	digcoeff := stationxml.Coefficients{
		BaseFilter: stationxml.BaseFilter{
			InputUnit:  inputUnit,
			OutputUnit: outputUnit},
		CfTransferFunctionType: "DIGITAL"}

	gain := stationxml.Gain{
		Value:     d.Sensitivity(),
		Frequency: 0}
	stage := stationxml.Stage{
		Number:       nextStageIndex,
		Decimation:   &dig,
		Coefficients: &digcoeff,
		StageGain:    &gain}
	result = append(result, stage)
	return
}

//
// LoadDigitizer load digitizer response from response databank
//
// respRoot          -- The response databank root directory
// digitizerFilePath -- The file path of digitizer response
// getUnit           -- Function to retreive abbreviation.Unit from unit code
//
// return            -- The staged digitizer
//
func LoadDigitizer(respRoot string, digitizerFilePath string,
	getXMLUnit func(label string) (stationxml.Unit, error),
	validator PzValidator) (result Staged, err error) {
	content, lerr := ioutil.ReadFile(path.Join(respRoot, digitizerFilePath))
	if lerr != nil {
		err = fmt.Errorf("Error reading %s: %s", digitizerFilePath, lerr.Error())
	}
	err = validator.DigitizerValidation(string(content))
	if err != nil {
		return
	}
	digitizer := new(Digitizer)
	digitizer.getXMLUnit = getXMLUnit
	lerr = json.Unmarshal(content, digitizer)
	if lerr != nil {
		err = fmt.Errorf("Error decoding %s: %s", digitizerFilePath, lerr.Error())
	}
	lerr = digitizer.isValid()
	if lerr != nil {
		err = fmt.Errorf("Error in %s: %s", digitizerFilePath, lerr.Error())
	}
	result = digitizer
	return
}

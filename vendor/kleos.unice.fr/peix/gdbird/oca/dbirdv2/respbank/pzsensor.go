package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"

	"kleos.unice.fr/peix/gseed/oca/seed/response"
	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// PZResponse define define specific part of pz sensor response
//
type PZResponse struct {
	TransferNormalizationFrequency *float64
	transferNormalizationFrequency float64
	Sensitivity                    *float64
	NoDamped                       bool
	PolesDef                       [][2]float64
	ZeroesDef                      [][2]float64
	PolesErrorDef                  [][2]float64
	ZeroesErrorDef                 [][2]float64
}

//
// PZSensor define poles/zeroes sensor response
//
type PZSensor struct {
	AbstractSensor
	PZResponse
}

//
// PZSensorInclude define include content of poles/zeroes sensor
//
type PZSensorInclude struct {
	PolesDef       [][2]float64
	ZeroesDef      [][2]float64
	PolesErrorDef  [][2]float64
	ZeroesErrorDef [][2]float64
}

//
// loadInclude loads include file if defined
//
func (s *PZSensor) loadInclude(respRoot string) (err error) {
	if s.Include == nil {
		return
	}
	content, lerr := ioutil.ReadFile(path.Join(respRoot, *s.Include))
	if lerr != nil {
		return lerr
	}
	include := new(PZSensorInclude)
	err = json.Unmarshal(content, include)
	if err != nil {
		return err
	}
	// TODO: Verify that we don't make redefinition
	if include.PolesDef != nil {
		s.PolesDef = include.PolesDef
	}
	if include.PolesErrorDef != nil {
		s.PolesErrorDef = include.PolesErrorDef
	}
	if include.ZeroesDef != nil {
		s.ZeroesDef = include.ZeroesDef
	}
	if include.ZeroesErrorDef != nil {
		s.ZeroesErrorDef = include.ZeroesErrorDef
	}
	return
}

func (s *PZSensor) isValid() error {
	// Verify that TransferNormalizationFrequency and Sensitivity is defined
	if s.TransferNormalizationFrequency == nil {
		return fmt.Errorf("TransferNormalizationFrequency is not defined")
	}
	if s.PZResponse.Sensitivity == nil {
		return fmt.Errorf("Sensitivity is not defined")
	}
	// Verify that if PolesErrorDef is defined it have same size that PolesDef
	if s.PolesErrorDef != nil && len(s.PolesErrorDef) != len(s.PolesDef) {
		return fmt.Errorf("PolesDef and PolesErrorDef must have same size")
	}
	// Verify that if ZeroesErrorDef is defined it have same size that ZeroesDef
	if s.ZeroesErrorDef != nil && len(s.ZeroesErrorDef) != len(s.ZeroesDef) {
		return fmt.Errorf("ZeroesDef and ZeroesErrorDef must have same size")
	}
	if err := checkRoots(s.zeroes(), s.TransferFunctionType, ZEROES, !s.NoDamped); err != nil {
		return fmt.Errorf("checkRoots failed for zeroes: %s", err.Error())
	}
	if err := checkRoots(s.poles(), s.TransferFunctionType, POLES, !s.NoDamped); err != nil {
		return fmt.Errorf("checkRoots failed for poles: %s", err.Error())
	}
	return nil
}

//
// poles give the poles slice of this sensor
//
func (s *PZSensor) poles() (result []complex128) {
	for _, current := range s.PolesDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

//
// zeroes give the zeroes slice of this sensor
//
func (s *PZSensor) zeroes() (result []complex128) {
	for _, current := range s.ZeroesDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

//
// FixTransferNormalizationFrequency fix (if need) the value of TransferNormalizationFrequency
// to respect the rule SampleRate <=  *s.TransferNormalizationFrequency / 2
//
func (s *PZSensor) FixTransferNormalizationFrequency(channelSampleRate float64) {
	s.transferNormalizationFrequency = *s.TransferNormalizationFrequency
	if *s.TransferNormalizationFrequency > channelSampleRate/2 {
		*s.TransferNormalizationFrequency = channelSampleRate / 2
	}
}

//
// ResetTransferNormalizationFrequency restore the value of TransferNormalizationFrequency
// to the value present in PZ file.
//
func (s *PZSensor) ResetTransferNormalizationFrequency() {
	*s.TransferNormalizationFrequency = s.transferNormalizationFrequency
}

//
// Response TODO
//
func (s *PZSensor) Response() (result stationxml.Response, err error) {
	var inputUnit, outputUnit stationxml.Unit
	gain := stationxml.Gain{Value: s.Sensitivity(),
		Frequency: *s.PZResponse.TransferNormalizationFrequency}
	inputUnit, err = s.getXMLUnit(*s.AbstractSensor.InputUnit)
	if err != nil {
		return
	}
	outputUnit, err = s.getXMLUnit("COUNTS")
	if err != nil {
		return
	}
	sensitivity := stationxml.Sensitivity{Gain: gain, InputUnits: inputUnit, OutputUnits: outputUnit}
	result = stationxml.Response{InstrumentSensitivity: &sensitivity,
		InstrumentPolynomial: nil, Stage: nil}
	return
}

func (s *PZSensor) xmlTransferFunctionType() (result string, err error) {
	if s.TransferFunctionType == "ANALOG" {
		result = "LAPLACE (HERTZ)"
	} else if s.TransferFunctionType == "LAPLACE" {
		result = "LAPLACE (RADIANS/SECOND)"
	} else if s.TransferFunctionType == "DIGITAL" {
		result = "DIGITAL (Z-TRANSFORM)"
	} else {
		err = fmt.Errorf("%s is an unknown PZ transfer function type",
			s.TransferFunctionType)
	}
	return
}

//
// XMLStage give the XML stages associated with this PZSensor
//
func (s *PZSensor) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	inputUnit, erri := s.getXMLUnit(*s.InputUnit)
	if erri != nil {
		err = fmt.Errorf("Error converting %s: %s", *s.InputUnit, erri.Error())
		return
	}
	outputUnit, erro := s.getXMLUnit(*s.OutputUnit)
	if erro != nil {
		err = fmt.Errorf("Error converting %s: %s", *s.OutputUnit, erro.Error())
		return
	}
	a0 := response.CalcAnalogA0(s.zeroes(), s.poles(), *s.TransferNormalizationFrequency, s.TransferFunctionType)
	var pzTransferFunctionType string
	pzTransferFunctionType, err = s.xmlTransferFunctionType()
	if err != nil {
		return
	}
	pz := stationxml.PolesZeros{
		BaseFilter: stationxml.BaseFilter{
			InputUnit:  inputUnit,
			OutputUnit: outputUnit},
		NormalizationFactor:    a0,
		PzTransferFunctionType: pzTransferFunctionType,
		NormalizationFrequency: *s.TransferNormalizationFrequency,
	}
	// Add zeros
	for index, current := range s.zeroes() {
		var currentError *complex128
		if s.ZeroesErrorDef != nil {
			tmp := complex(s.ZeroesErrorDef[index][0], s.ZeroesErrorDef[index][1])
			currentError = &tmp
		}
		pz.Zero = append(pz.Zero, CreatePoleZero(index, current, currentError))
	}
	// Add poles
	for index, current := range s.poles() {
		var currentError *complex128
		if s.PolesErrorDef != nil {
			tmp := complex(s.PolesErrorDef[index][0], s.PolesErrorDef[index][1])
			currentError = &tmp
		}
		pz.Pole = append(pz.Pole, CreatePoleZero(index, current, currentError))
	}
	gain := stationxml.Gain{Value: s.Sensitivity(),
		Frequency: *s.TransferNormalizationFrequency}
	stage := stationxml.Stage{Number: nextStageIndex, PolesZeros: &pz,
		StageGain: &gain}
	result = append(result, stage)
	return
}

//
// Sensitivity give the sensitivity of this pzSensor
//
func (s *PZSensor) Sensitivity() float64 {
	return *s.PZResponse.Sensitivity
}

func loadPZSensor(respRoot string, sensorFilePath string, abstract AbstractSensor, jsonStr []byte) (result *PZSensor, err error) {
	var lerr error
	pzResp := new(PZResponse)
	lerr = json.Unmarshal(jsonStr, pzResp)
	if lerr != nil {
		err = fmt.Errorf("error decoding %s: %s", sensorFilePath, lerr.Error())
		return
	}
	result = &PZSensor{abstract, *pzResp}
	lerr = result.loadInclude(respRoot)
	if lerr != nil {
		err = fmt.Errorf("error when loading include %s from %s: %s",
			*result.Include, sensorFilePath, lerr.Error())
		return
	}
	lerr = result.isValid()
	if lerr != nil {
		err = fmt.Errorf("error in %s: %s", sensorFilePath, lerr.Error())
		return
	}
	return
}

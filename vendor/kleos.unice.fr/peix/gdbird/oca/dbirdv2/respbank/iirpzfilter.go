package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"

	"kleos.unice.fr/peix/gseed/oca/seed/response"
	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// IIRPZResponse define define specific part of iir pz filter response
//
type IIRPZResponse struct {
	PolesDef       [][2]float64
	ZeroesDef      [][2]float64
	PolesErrorDef  [][2]float64
	ZeroesErrorDef [][2]float64
}

//
// IIRPZFilter define IIR wth poles/zeroes digital filtrer response
//
type IIRPZFilter struct {
	AbstractDigitalFilter
	IIRPZResponse
}

//
// IIRPZFilterInclude define include content of iir pz filter
//
type IIRPZFilterInclude struct {
	PolesDef       [][2]float64
	ZeroesDef      [][2]float64
	PolesErrorDef  [][2]float64
	ZeroesErrorDef [][2]float64
}

func (f *IIRPZFilter) loadInclude(respRoot string) error {
	var err error
	if f.Include == nil {
		return nil
	}

	content, lerr := ioutil.ReadFile(path.Join(respRoot, *f.Include))
	if lerr != nil {
		return lerr
	}
	include := new(IIRPZFilterInclude)
	err = json.Unmarshal(content, include)
	if err != nil {
		return err
	}
	// TODO: Verify that we don't make redefinition
	if include.PolesDef != nil {
		f.PolesDef = include.PolesDef
	}
	if include.PolesErrorDef != nil {
		f.PolesErrorDef = include.PolesErrorDef
	}
	if include.ZeroesDef != nil {
		f.ZeroesDef = include.ZeroesDef
	}
	if include.ZeroesErrorDef != nil {
		f.ZeroesErrorDef = include.ZeroesErrorDef
	}
	return nil
}

func (f *IIRPZFilter) isValid() error {
	// Verify that if PolesErrorDef is defined it have same size that PolesDef
	if f.PolesErrorDef != nil && len(f.PolesErrorDef) != len(f.PolesDef) {
		return fmt.Errorf("PolesDef and PolesErrorDef must have same size")
	}
	// Verify that if ZeroesErrorDef is defined it have same size that ZeroesDef
	if f.ZeroesErrorDef != nil && len(f.ZeroesErrorDef) != len(f.ZeroesDef) {
		return fmt.Errorf("ZeroesDef and ZeroesErrorDef must have same size")
	}
	if err := checkRoots(f.zeroes(), IIRPZ, ZEROES, false); err != nil {
		return fmt.Errorf("checkRoots failed for zeroes: %s", err.Error())
	}
	if err := checkRoots(f.poles(), IIRPZ, POLES, false); err != nil {
		return fmt.Errorf("checkRoots failed for poles: %s", err.Error())
	}
	return nil
}

//
// poles give the poles slice of this filter
//
func (f *IIRPZFilter) poles() (result []complex128) {
	for _, current := range f.PolesDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

//
// polesError give the poles error slice of this filter
//
//func (f *IIRPZFilter) polesError() (result []complex128) {
//	if f.PolesErrorDef == nil && len(f.PolesDef) != 0 {
//		return make([]complex128, len(f.PolesDef))
//	}
//	for _, current := range f.PolesErrorDef {
//		result = append(result, complex(current[0], current[1]))
//	}
//	return
//}

//
// Zeroes give the zeroes slice of this filter
//
func (f *IIRPZFilter) zeroes() (result []complex128) {
	for _, current := range f.ZeroesDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

//
// zeroesError give the zeroes error slice of this filter
//
//nc (f *IIRPZFilter) zeroesError() (result []complex128) {
//	if f.ZeroesErrorDef == nil && len(f.ZeroesDef) != 0 {
//		return make([]complex128, len(f.ZeroesDef))
//	}
//	for _, current := range f.ZeroesErrorDef {
//		result = append(result, complex(current[0], current[1]))
//	}
//	return
//

//
// XMLStage give the XML stages associated with this PZSensor
//
func (f *IIRPZFilter) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	inputUnit, erri := f.getXMLUnit("COUNTS")
	if erri != nil {
		err = fmt.Errorf("Error converting %s: %s", "COUNTS", erri.Error())
		return
	}
	outputUnit, erro := f.getXMLUnit("COUNTS")
	if erro != nil {
		err = fmt.Errorf("Error converting %s: %s", "COUNTS", erro.Error())
		return
	}
	a0 := response.CalcIirPzA0(f.zeroes(), f.poles(), *f.FrequencyOfGain, 1 / *f.InputSamplingRate)
	pz := stationxml.PolesZeros{
		BaseFilter: stationxml.BaseFilter{
			InputUnit:  inputUnit,
			OutputUnit: outputUnit},
		NormalizationFrequency: *f.FrequencyOfGain,
		NormalizationFactor:    a0,
		PzTransferFunctionType: "DIGITAL (Z-TRANSFORM)"}

	// Add zeros
	for index, current := range f.zeroes() {
		var currentError *complex128
		if f.ZeroesErrorDef != nil {
			tmp := complex(f.ZeroesErrorDef[index][0], f.ZeroesErrorDef[index][1])
			currentError = &tmp
		}
		pz.Zero = append(pz.Zero, CreatePoleZero(index, current, currentError))
	}
	// Add poles
	for index, current := range f.poles() {
		var currentError *complex128
		if f.PolesErrorDef != nil {
			tmp := complex(f.PolesErrorDef[index][0], f.PolesErrorDef[index][1])
			currentError = &tmp
		}
		pz.Pole = append(pz.Pole, CreatePoleZero(index, current, currentError))
	}
	decimation := f.decimation()
	gain := f.gain()
	stage := stationxml.Stage{Number: nextStageIndex, PolesZeros: &pz,
		Decimation: &decimation, StageGain: &gain}
	result = append(result, stage)

	return
}

func loadIIRPZFilter(respRoot string, filterFilePath string, abstract AbstractDigitalFilter, jsonStr []byte) (result *IIRPZFilter, err error) {
	iirPzResponse := new(IIRPZResponse)
	lerr := json.Unmarshal(jsonStr, iirPzResponse)
	if lerr != nil {
		err = fmt.Errorf("error decoding %s: %s", filterFilePath, lerr.Error())
		return
	}
	result = &IIRPZFilter{abstract, *iirPzResponse}
	lerr = result.loadInclude(respRoot)
	if lerr != nil {
		err = fmt.Errorf("error when loading include %s from %s: %s",
			*result.Include, filterFilePath, lerr.Error())
		return
	}
	lerr = result.isValid()
	if lerr != nil {
		err = fmt.Errorf("error in %s: %s", filterFilePath, lerr.Error())
		return
	}
	return
}

//
// Sensitivity give the sensitivity of the iirpzFilter
//
func (f *IIRPZFilter) Sensitivity() float64 {
	return *f.AbstractDigitalFilter.Sensitivity
}

package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"strings"

	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// JSONSensor define type used to load json file from response databank
//
type JSONSensor struct {
	AbstractSensor
	Response *json.RawMessage
}

//
// AbstractSensor define common part of sensor response
//
type AbstractSensor struct {
	getXMLUnit           func(label string) (stationxml.Unit, error)
	TransferFunctionType string
	InputUnit            *string
	OutputUnit           *string
	Include              *string
}

func (s *JSONSensor) isValid() error {
	if s.InputUnit == nil {
		return fmt.Errorf("missing InputUnit")
	}
	if s.OutputUnit == nil {
		return fmt.Errorf("missing OutputUnit")
	}
	return nil
}

//
// LoadSensor load sensor from response databank
//
// respRoot       -- The response databank root directory
// sensorFilePath -- The sensor file path
// getUnit        -- Function to retreive abbreviation.Unit from unit code
//
// return         -- A staged object
//
func LoadSensor(respRoot string, sensorFilePath string,
	getXMLUnit func(label string) (stationxml.Unit, error),
	validator PzValidator) (result Staged, err error) {
	content, lerr := ioutil.ReadFile(path.Join(respRoot, sensorFilePath))
	if lerr != nil {
		err = fmt.Errorf("Error reading %s: %s", sensorFilePath, lerr.Error())
		return
	}
	err = validator.SensorValidation(string(content))
	if err != nil {
		return
	}
	jsonSensor := new(JSONSensor)
	lerr = json.Unmarshal(content, jsonSensor)
	if lerr != nil {
		err = fmt.Errorf("Error decoding %s: %s", sensorFilePath, lerr.Error())
		return
	}
	lerr = jsonSensor.isValid()
	if lerr != nil {
		err = fmt.Errorf("%s in %s", lerr.Error(), sensorFilePath)
		return
	}
	if strings.Compare(jsonSensor.TransferFunctionType, ANALOG) == 0 ||
		strings.Compare(jsonSensor.TransferFunctionType, LAPLACE) == 0 {
		var pzSensor *PZSensor
		pzSensor, err = loadPZSensor(respRoot, sensorFilePath, jsonSensor.AbstractSensor,
			*jsonSensor.Response)
		if err != nil {
			return
		}
		pzSensor.getXMLUnit = getXMLUnit
		result = pzSensor
	} else if strings.Compare(jsonSensor.TransferFunctionType, POLYNOMIAL) == 0 {
		var polySensor *PolynomialSensor
		polySensor, err = loadPolynomialSensor(respRoot, sensorFilePath, jsonSensor.AbstractSensor,
			*jsonSensor.Response)
		if err != nil {
			return
		}
		polySensor.getXMLUnit = getXMLUnit
		result = polySensor
	} else {
		err = fmt.Errorf("Unknown transfer function type %s in %s",
			jsonSensor.TransferFunctionType, sensorFilePath)
		return
	}
	return
}

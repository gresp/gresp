package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"

	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// IIRCoefResponse define define specific part of iir coefficients filter response
//
type IIRCoefResponse struct {
	Numerators        []float64
	NumeratorsError   []float64
	Denominators      []float64
	DenominatorsError []float64
}

//
// IIRCoefFilter define IIR coefficients digital filtrer response
//
type IIRCoefFilter struct {
	AbstractDigitalFilter
	IIRCoefResponse
}

//
// IIRCoefFilterInclude define include content of iir coefficient filter
//
type IIRCoefFilterInclude struct {
	Numerators        []float64
	NumeratorsError   []float64
	Denominators      []float64
	DenominatorsError []float64
}

//
// loadInclude loads include file if defined
//
func (f *IIRCoefFilter) loadInclude(respRoot string) (err error) {
	if f.Include == nil {
		return
	}
	content, lerr := ioutil.ReadFile(path.Join(respRoot, *f.Include))
	if lerr != nil {
		return lerr
	}
	include := new(IIRCoefFilterInclude)
	err = json.Unmarshal(content, include)
	if err != nil {
		return err
	}
	// TODO: Verify that we don't make redefinition
	if include.Numerators != nil {
		f.Numerators = include.Numerators
	}
	if include.NumeratorsError != nil {
		f.NumeratorsError = include.NumeratorsError
	}
	if include.Denominators != nil {
		f.Denominators = include.Denominators
	}
	if include.DenominatorsError != nil {
		f.DenominatorsError = include.DenominatorsError
	}
	return
}

func (f *IIRCoefFilter) isValid() error {
	// Verify that if PolesErrorDef is defined it have same size that PolesDef
	if f.NumeratorsError != nil && len(f.NumeratorsError) != len(f.Numerators) {
		return fmt.Errorf("numerators and NumeratorsError must have same size")
	}
	// Verify that if ZeroesErrorDef is defined it have same size that ZeroesDef
	if f.DenominatorsError != nil && len(f.DenominatorsError) != len(f.Denominators) {
		return fmt.Errorf("denominators and DenominatorsError must have same size")
	}
	return nil
}

//
// XMLStage give the XML stages associated with this PZSensor
//
func (f *IIRCoefFilter) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	inputUnit, erri := f.getXMLUnit("COUNTS")
	if erri != nil {
		err = fmt.Errorf("Error converting %s: %s", "COUNTS", erri.Error())
		return
	}
	outputUnit, erro := f.getXMLUnit("COUNTS")
	if erro != nil {
		err = fmt.Errorf("Error converting %s: %s", "COUNTS", erro.Error())
		return
	}
	coef := stationxml.Coefficients{
		BaseFilter: stationxml.BaseFilter{
			InputUnit:  inputUnit,
			OutputUnit: outputUnit},
		CfTransferFunctionType: "DIGITAL"}

	for index, current := range f.IIRCoefResponse.Numerators {
		var minError, maxError *float64
		if f.IIRCoefResponse.NumeratorsError != nil {
			minError = &(f.IIRCoefResponse.NumeratorsError[index])
			maxError = &(f.IIRCoefResponse.NumeratorsError[index])
		}
		currentCoef := stationxml.Coefficient{
			Number: index,
			FloatNoUnit: stationxml.FloatNoUnit{
				Value:    current,
				MinError: minError,
				MaxError: maxError}}

		coef.Numerator = append(coef.Numerator, currentCoef)
	}
	for index, current := range f.IIRCoefResponse.Denominators {
		var minError, maxError *float64
		if f.IIRCoefResponse.DenominatorsError != nil {
			minError = &(f.IIRCoefResponse.DenominatorsError[index])
			maxError = &(f.IIRCoefResponse.DenominatorsError[index])
		}
		currentCoef := stationxml.Coefficient{
			Number: index,
			FloatNoUnit: stationxml.FloatNoUnit{
				Value:    current,
				MinError: minError,
				MaxError: maxError}}
		coef.Denominator = append(coef.Denominator, currentCoef)
	}
	decimation := f.decimation()
	gain := f.gain()
	stage := stationxml.Stage{Number: nextStageIndex, Coefficients: &coef,
		Decimation: &decimation, StageGain: &gain}
	result = append(result, stage)
	return
}

func loadIIRCoefFilter(respRoot string, filterFilePath string, abstract AbstractDigitalFilter, jsonStr []byte) (result *IIRCoefFilter, err error) {
	iirCoefResponse := new(IIRCoefResponse)
	lerr := json.Unmarshal(jsonStr, iirCoefResponse)
	if lerr != nil {
		err = fmt.Errorf("error decoding %s: %s", filterFilePath, lerr.Error())
		return
	}
	result = &IIRCoefFilter{abstract, *iirCoefResponse}
	lerr = result.loadInclude(respRoot)
	if lerr != nil {
		err = fmt.Errorf("error when loading include %s from %s: %s",
			*result.Include, filterFilePath, lerr.Error())
		return
	}
	lerr = result.isValid()
	if lerr != nil {
		err = fmt.Errorf("error in %s: %s", filterFilePath, lerr.Error())
		return
	}
	return
}

//
// Sensitivity give the sensitivity of the IIRCoefFilter
//
func (f *IIRCoefFilter) Sensitivity() float64 {
	return *f.AbstractDigitalFilter.Sensitivity
}

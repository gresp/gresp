package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"strings"

	"kleos.unice.fr/peix/gseed/oca/seed/response"
	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

//
// AnalogFilterCascade define cascading of analog filter
// (The content of response databank files)
//
type AnalogFilterCascade []*AnaFilter

//
// XMLStage give the *stationxml.Stage slice associated with this AnaFilterCascade
//
func (fc AnalogFilterCascade) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	for _, currentFilter := range fc {
		var currentStage []stationxml.Stage
		currentStage, err = currentFilter.XMLStage(nextStageIndex)
		if err != nil {
			return
		}
		result = append(result, currentStage...)
		nextStageIndex++
	}
	return
}

//
// Sensitivity give the global sensitivity of this AnaFilterCascade
//
func (fc AnalogFilterCascade) Sensitivity() (result float64) {
	result = 1
	for _, currentFilter := range fc {
		result *= *(currentFilter.Response.Sensitivity)
	}
	return
}

//
// AnaFilterPZResponse define analog filter poles.zeroes response
//
type AnaFilterPZResponse struct {
	TransferNormalizationFrequency *float64
	Sensitivity                    *float64
	PolesDef                       [][2]float64
	ZeroesDef                      [][2]float64
	PolesErrorDef                  [][2]float64
	ZeroesErrorDef                 [][2]float64
}

//
// AnaFilter define analog filter response
//
type AnaFilter struct {
	TransferFunctionType string
	InputUnit            *string
	OutputUnit           *string
	Response             *AnaFilterPZResponse
	Include              *string
	getXMLUnit           func(label string) (stationxml.Unit, error)
}

//
// AnaFilterInclude define include content of analog filter
//
type AnaFilterInclude struct {
	PolesDef       [][2]float64
	ZeroesDef      [][2]float64
	PolesErrorDef  [][2]float64
	ZeroesErrorDef [][2]float64
}

//
// loadInclude loads include file if defined
//
func (f *AnaFilter) loadInclude(respRoot string) (err error) {
	if f.Include == nil {
		return
	}
	content, lerr := ioutil.ReadFile(path.Join(respRoot, *f.Include))
	if lerr != nil {
		err = lerr
		return
	}
	include := new(AnaFilterInclude)
	err = json.Unmarshal(content, include)
	if err != nil {
		return err
	}
	// TODO: Verify that we don't make redefinition
	if include.PolesDef != nil {
		f.Response.PolesDef = include.PolesDef
	}
	if include.PolesErrorDef != nil {
		f.Response.PolesErrorDef = include.PolesErrorDef
	}
	if include.ZeroesDef != nil {
		f.Response.ZeroesDef = include.ZeroesDef
	}
	if include.ZeroesErrorDef != nil {
		f.Response.ZeroesErrorDef = include.ZeroesErrorDef
	}
	return
}

//
// isValid say if this AnaFilter definition is valid
//
func (f *AnaFilter) isValid() error {
	if strings.Compare(f.TransferFunctionType, ANALOG) != 0 &&
		strings.Compare(f.TransferFunctionType, LAPLACE) != 0 {
		return fmt.Errorf("unknown transfer function type %s", f.TransferFunctionType)
	}
	if f.InputUnit == nil {
		return fmt.Errorf("missing InputUnit")
	}
	if f.OutputUnit == nil {
		return fmt.Errorf("missing OutputUnit")
	}
	if f.Response.Sensitivity == nil {
		return fmt.Errorf("missing Sensitivity")
	}

	if f.Response.TransferNormalizationFrequency == nil {
		return fmt.Errorf("missing TransferNormalizationFrequency")
	}
	// Verify that if PolesErrorDef is defined it have same size that PolesDef
	if f.Response.PolesErrorDef != nil && len(f.Response.PolesErrorDef) != len(f.Response.PolesDef) {
		return fmt.Errorf("polesDef and PolesErrorDef must have same size")
	}
	// Verify that if ZeroesErrorDef is defined it have same size that ZeroesDef
	if f.Response.ZeroesErrorDef != nil && len(f.Response.ZeroesErrorDef) != len(f.Response.ZeroesDef) {
		return fmt.Errorf("zeroesDef and ZeroesErrorDef must have same size")
	}
	if err := checkRoots(f.Zeroes(), f.TransferFunctionType, ZEROES, false); err != nil {
		return fmt.Errorf("checkRoots failed for zeroes: %s", err.Error())
	}
	if err := checkRoots(f.Poles(), f.TransferFunctionType, POLES, false); err != nil {
		return fmt.Errorf("checkRoots failed for poles: %s", err.Error())
	}
	return nil
}

//
// FixNormalizationFrequency fix the normalization frequency of this analog filter
//
// newValue -- The new value of normalization frequency
//
//
func (f *AnaFilter) FixNormalizationFrequency(newValue *float64) {
	var defaultValue = 1.0
	if newValue == nil {
		f.Response.TransferNormalizationFrequency = &defaultValue
	} else {
		f.Response.TransferNormalizationFrequency = newValue
	}
}

//
// Poles give the poles slice of this analog filter
//
func (f *AnaFilter) Poles() (result []complex128) {
	for _, current := range f.Response.PolesDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

//
// PolesError give the poles error slice of this analog filter
//
func (f *AnaFilter) PolesError() (result []complex128) {
	if f.Response.PolesErrorDef == nil && len(f.Response.PolesDef) != 0 {
		return make([]complex128, len(f.Response.PolesDef))
	}
	for _, current := range f.Response.PolesErrorDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

//
// Zeroes give the zeroes slice of this analog filter
//
func (f *AnaFilter) Zeroes() (result []complex128) {
	for _, current := range f.Response.ZeroesDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

//
// ZeroesError give the zeroes error slice of this analog filter
//
func (f *AnaFilter) ZeroesError() (result []complex128) {
	if f.Response.ZeroesErrorDef == nil && len(f.Response.ZeroesDef) != 0 {
		return make([]complex128, len(f.Response.ZeroesDef))
	}
	for _, current := range f.Response.ZeroesErrorDef {
		result = append(result, complex(current[0], current[1]))
	}
	return
}

func (f *AnaFilter) xmlTransferFunctionType() (result string, err error) {
	if f.TransferFunctionType == "ANALOG" {
		result = "LAPLACE (HERTZ)"
	} else if f.TransferFunctionType == "LAPLACE" {
		result = "LAPLACE (RADIANS/SECOND)"
	} else if f.TransferFunctionType == "DIGITAL" {
		result = "DIGITAL (Z-TRANSFORM)"
	} else {
		err = fmt.Errorf("%s is an unknown PZ transfer function type",
			f.TransferFunctionType)
	}
	return
}

//
// XMLStage give the stationxml.Stage associated with this AnaFilter
//
func (f *AnaFilter) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {
	inputUnit, erri := f.getXMLUnit(*f.InputUnit)
	if erri != nil {
		err = fmt.Errorf("Error converting %s: %s", *f.InputUnit, erri.Error())
		return
	}
	outputUnit, erro := f.getXMLUnit(*f.OutputUnit)
	if erro != nil {
		err = fmt.Errorf("Error converting %s: %s", *f.OutputUnit, erro.Error())
		return
	}

	if len(f.Poles()) != 0 || len(f.Zeroes()) != 0 {
		a0 := response.CalcAnalogA0(f.Zeroes(), f.Poles(),
			*f.Response.TransferNormalizationFrequency, f.TransferFunctionType)
		var pzTransferFunctionType string
		pzTransferFunctionType, err = f.xmlTransferFunctionType()
		if err != nil {
			return
		}
		pz := stationxml.PolesZeros{
			BaseFilter: stationxml.BaseFilter{
				InputUnit:  inputUnit,
				OutputUnit: outputUnit},
			NormalizationFactor:    a0,
			PzTransferFunctionType: pzTransferFunctionType,
			NormalizationFrequency: *f.Response.TransferNormalizationFrequency}

		// Add zeros
		for index, current := range f.Zeroes() {
			var currentError *complex128
			if f.Response.ZeroesErrorDef != nil {
				tmp := complex(f.Response.ZeroesErrorDef[index][0], f.Response.ZeroesErrorDef[index][1])
				currentError = &tmp
			}
			pz.Zero = append(pz.Zero, CreatePoleZero(index, current, currentError))
		}
		// Add poles
		for index, current := range f.Poles() {
			var currentError *complex128
			if f.Response.PolesErrorDef != nil {
				tmp := complex(f.Response.PolesErrorDef[index][0], f.Response.PolesErrorDef[index][1])
				currentError = &tmp
			}
			pz.Pole = append(pz.Pole, CreatePoleZero(index, current, currentError))
		}
		gain := &stationxml.Gain{Value: *f.Response.Sensitivity,
			Frequency: *f.Response.TransferNormalizationFrequency}
		stage := stationxml.Stage{Number: nextStageIndex, PolesZeros: &pz,
			StageGain: gain}
		result = append(result, stage)

	} else {
		gain := stationxml.Gain{Value: *f.Response.Sensitivity,
			Frequency: *f.Response.TransferNormalizationFrequency}
		stage := stationxml.Stage{Number: nextStageIndex,
			StageGain: &gain}
		result = append(result, stage)
	}
	return
}

//
// LoadAnalogFilter load analog filter response from response databank
//
// respRoot       -- The response databank root directory
// filterFilePath -- The file path of filter response
// getUnit        -- Function to retreive abbreviation.Unit from unit code
// getXMLUnit     -- Function to retreive stationXML.Unit from unit code
// return         -- The staged analog filter cascade
//
func LoadAnalogFilter(respRoot string, filterFilePath string,
	getXMLUnit func(label string) (stationxml.Unit, error),
	validator PzValidator) (result Staged, err error) {
	content, lerr := ioutil.ReadFile(path.Join(respRoot, filterFilePath))
	if lerr != nil {
		err = fmt.Errorf("Error reading %s: %s", filterFilePath, lerr.Error())
		return
	}
	err = validator.AnalogFilterValidation(string(content))
	if err != nil {
		return
	}
	filterCascade := new(AnalogFilterCascade)
	lerr = json.Unmarshal(content, filterCascade)
	if lerr != nil {
		err = fmt.Errorf("Error decoding %s: %s", filterFilePath, lerr.Error())
		return
	}
	for _, currentFilter := range *filterCascade {
		lerr = currentFilter.loadInclude(respRoot)
		if lerr != nil {
			err = fmt.Errorf("Error when loading include %s from %s: %s",
				*currentFilter.Include, filterFilePath, lerr.Error())
			return
		}
		if (currentFilter.Response.TransferNormalizationFrequency == nil || *currentFilter.Response.TransferNormalizationFrequency != 0) &&
			len(currentFilter.Response.PolesDef) == 0 && len(currentFilter.Response.ZeroesDef) == 0 {
			currentFilter.FixNormalizationFrequency(nil)
		}
		lerr = currentFilter.isValid()
		if lerr != nil {
			err = fmt.Errorf("Error in %s: %s", filterFilePath, lerr.Error())
			return
		}
		currentFilter.getXMLUnit = getXMLUnit
	}
	result = filterCascade
	return
}

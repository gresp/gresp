package respbank

import (
	"fmt"
	"path"
	"path/filepath"

	"github.com/xeipuuv/gojsonschema"
)

// PzValidator is used to validate PZ files
type PzValidator struct {
	sensorSchema        *gojsonschema.Schema
	analogFilterSchema  *gojsonschema.Schema
	digitizerSchema     *gojsonschema.Schema
	digitalFilterSchema *gojsonschema.Schema
}

func loadSchema(fpath string) (result *gojsonschema.Schema, err error) {
	absolutePath, lerr := filepath.Abs(fpath)
	if lerr != nil {
		err = fmt.Errorf("filepath.Abs error: %s", lerr.Error())
		return
	}
	schemaLoader := gojsonschema.NewReferenceLoader(fmt.Sprintf("file://%s", absolutePath))
	result, lerr = gojsonschema.NewSchema(schemaLoader)
	if lerr != nil {
		err = fmt.Errorf("Error during loading schema: %s", lerr.Error())
		return
	}
	return
}

// NewPzValidator create and initialize a new PzValidator
func NewPzValidator(schemaRootDir string) (result PzValidator, err error) {
	// Begin to load schema
	var sensorSchema, analogFilterSchema, digitizerSchema, digitalFilterSchema *gojsonschema.Schema
	sensorSchemaPath := path.Join(schemaRootDir, "sensor.json")
	sensorSchema, err = loadSchema(sensorSchemaPath)
	if err != nil {
		return
	}
	analogFilterSchemaPath := path.Join(schemaRootDir, "analog_filter.json")
	analogFilterSchema, err = loadSchema(analogFilterSchemaPath)
	if err != nil {
		return
	}

	digitizerSchemaPath := path.Join(schemaRootDir, "digitizer.json")
	digitizerSchema, err = loadSchema(digitizerSchemaPath)
	if err != nil {
		return
	}

	digitalFilterSchemaPath := path.Join(schemaRootDir, "analog_filter.json")
	digitalFilterSchema, err = loadSchema(digitalFilterSchemaPath)
	if err != nil {
		return
	}

	result = PzValidator{
		sensorSchema:        sensorSchema,
		analogFilterSchema:  analogFilterSchema,
		digitizerSchema:     digitizerSchema,
		digitalFilterSchema: digitalFilterSchema}
	return
}

func (v *PzValidator) validate(schema *gojsonschema.Schema, loader gojsonschema.JSONLoader) (valid bool, err error) {
	result, err := schema.Validate(loader)
	if err != nil {
		return
	}
	valid = result.Valid()
	if !valid {
		for _, err := range result.Errors() {
			fmt.Printf("- %s\n", err)
		}
	}
	return
}

// SensorValidation validate content with sensor schema
func (v *PzValidator) SensorValidation(content string) error {
	loader := gojsonschema.NewStringLoader(content)
	valid, err := v.validate(v.sensorSchema, loader)
	if err != nil {
		return fmt.Errorf("Error during sensor validation: %s", err.Error())
	}
	if !valid {
		return fmt.Errorf("Can't validate sensor")
	}
	return nil
}

// AnalogFilterValidation validate content with analog filter schema
func (v *PzValidator) AnalogFilterValidation(content string) error {
	loader := gojsonschema.NewStringLoader(content)
	valid, err := v.validate(v.analogFilterSchema, loader)
	if err != nil {
		return fmt.Errorf("Error during analog filter validation: %s", err.Error())
	}
	if !valid {
		return fmt.Errorf("Can't validate analog filter")
	}
	return nil
}

// DigitizerValidation validate content with digitizer schema
func (v *PzValidator) DigitizerValidation(content string) error {
	loader := gojsonschema.NewStringLoader(content)
	valid, err := v.validate(v.digitizerSchema, loader)
	if err != nil {
		return fmt.Errorf("Error during digitizer validation: %s", err.Error())
	}
	if !valid {
		return fmt.Errorf("Can't validate digitizer")
	}
	return nil
}

// DigitalFilterValidation validate content with digital filter schema
func (v *PzValidator) DigitalFilterValidation(content string) error {
	loader := gojsonschema.NewStringLoader(content)
	valid, err := v.validate(v.digitalFilterSchema, loader)
	if err != nil {
		return fmt.Errorf("Error during digital filter validation: %s", err.Error())
	}
	if !valid {
		return fmt.Errorf("Can't validate digital filter")
	}
	return nil
}

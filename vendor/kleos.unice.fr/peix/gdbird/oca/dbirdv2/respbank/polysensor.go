package respbank

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"

	"kleos.unice.fr/peix/stationxml/oca/stationxml"
)

const (
	// HZUnit is Hz valid frequency unit
	HZUnit = "Hz"
	// RADUnit rad/s valid frequency unit
	RADUnit = "rad/s"
)

//
// PolynomialResponse define specific part of polynomial sensor response
//
type PolynomialResponse struct {
	ValidFrequencyUnit       string
	LowerBoundApproximation  *float64
	UpperBoundApproximation  *float64
	LowerValidFrequencyBound *float64
	UpperValidFrequencyBound *float64
	MaximumAbsoluteError     float64
	Coefficients             []float64
	CoefficientsError        []float64
}

//
// PolynomialSensor define polynomial sensor response
//
type PolynomialSensor struct {
	AbstractSensor
	PolynomialResponse
}

//
// PolynomialSensorInclude define include content of polynomial sensor
//
type PolynomialSensorInclude struct {
	Coefficients      []float64
	CoefficientsError []float64
}

//
// loadInclude loads include file if defined
//
func (s *PolynomialSensor) loadInclude(respRoot string) (err error) {
	if s.Include == nil {
		return
	}
	content, lerr := ioutil.ReadFile(path.Join(respRoot, *s.Include))
	if lerr != nil {
		err = lerr
		return
	}
	include := new(PolynomialSensorInclude)
	err = json.Unmarshal(content, include)
	if err != nil {
		return err
	}
	//TODO: verify that we don't make redefinition
	if include.Coefficients != nil {
		s.Coefficients = include.Coefficients
	}
	if include.CoefficientsError != nil {
		s.CoefficientsError = include.CoefficientsError
	}
	return
}

func (s *PolynomialSensor) isValid() error {
	if s.ValidFrequencyUnit != HZUnit && s.ValidFrequencyUnit != RADUnit {
		return fmt.Errorf("ValidFrequencyUnit is %s and must be %s|%s",
			s.ValidFrequencyUnit, RADUnit, HZUnit)
	}
	if s.LowerBoundApproximation == nil {
		return fmt.Errorf("LowerBoundApproximation is nil")
	}
	if s.UpperBoundApproximation == nil {
		return fmt.Errorf("UpperBoundApproximation is nil")
	}
	if s.Coefficients == nil {
		return fmt.Errorf("coefficients is nil")
	}
	if s.CoefficientsError != nil && len(s.Coefficients) != len(s.CoefficientsError) {
		return fmt.Errorf("the Coefficients size (%d) must be equal to CoefficientsError size (%d)",
			len(s.Coefficients), len(s.CoefficientsError))
	}

	return nil
}

func (s *PolynomialSensor) lowerValidFrequencyBound() float64 {
	if s.LowerValidFrequencyBound == nil {
		return 0
	}
	return *s.LowerValidFrequencyBound
}

//
// UpperValidFrequency give the UpperValidFrequencyBound of this PolynomialSensor
//
func (s *PolynomialSensor) UpperValidFrequency() *float64 {
	return s.UpperValidFrequencyBound
}

func (s *PolynomialSensor) upperValidFrequencyBound() float64 {
	if s.UpperValidFrequencyBound == nil {
		return 0
	}
	return *s.UpperValidFrequencyBound
}

//
// Response TODO
//
func (s *PolynomialSensor) Response() (result stationxml.Response, err error) {
	var inputUnit, outputUnit stationxml.Unit

	inputUnit, err = s.getXMLUnit(*s.AbstractSensor.InputUnit)
	if err != nil {
		return
	}
	outputUnit, err = s.getXMLUnit("COUNTS")
	if err != nil {
		return
	}

	lowerFrequency := stationxml.Frequency{
		FloatNoUnit: stationxml.FloatNoUnit{
			Value: s.lowerValidFrequencyBound()},
		Unit: "HERTZ"}
	upperFrequency := stationxml.Frequency{
		FloatNoUnit: stationxml.FloatNoUnit{
			Value: s.upperValidFrequencyBound()},
		Unit: "HERTZ"}
	polyResp := stationxml.Polynomial{
		BaseFilter: stationxml.BaseFilter{
			InputUnit:  inputUnit,
			OutputUnit: outputUnit},
		ApproximationType:       "MACLAURIN",
		FrequencyLowerBound:     lowerFrequency,
		FrequencyUpperBound:     upperFrequency,
		ApproximationLowerBound: *s.PolynomialResponse.LowerBoundApproximation,
		ApproximationUpperBound: *s.PolynomialResponse.UpperBoundApproximation,
		MaximumError:            s.PolynomialResponse.MaximumAbsoluteError}

	// Add Coefficients
	for index, current := range s.Coefficients {
		var currentError *float64
		if s.CoefficientsError != nil {
			tmp := s.CoefficientsError[index]
			currentError = &tmp
		}
		polyResp.Coefficient = append(polyResp.Coefficient,
			stationxml.CreateCoefficient(index, current, currentError))
	}

	result = stationxml.Response{InstrumentSensitivity: nil,
		InstrumentPolynomial: &polyResp, Stage: nil}
	return
}

//
// XMLStage give the XML stages associated with this PZSensor
//
func (s *PolynomialSensor) XMLStage(nextStageIndex int) (result []stationxml.Stage, err error) {

	inputUnit, erri := s.getXMLUnit(*s.InputUnit)
	if erri != nil {
		err = fmt.Errorf("Error converting %s: %s", *s.InputUnit, erri.Error())
		return
	}
	outputUnit, erro := s.getXMLUnit(*s.OutputUnit)
	if erro != nil {
		err = fmt.Errorf("Error converting %s: %s", *s.OutputUnit, erro.Error())
		return
	}

	lowerFrequency := stationxml.Frequency{
		FloatNoUnit: stationxml.FloatNoUnit{
			Value: s.lowerValidFrequencyBound()},
		Unit: "HERTZ"}
	upperFrequency := stationxml.Frequency{
		FloatNoUnit: stationxml.FloatNoUnit{
			Value: s.lowerValidFrequencyBound()},
		Unit: "HERTZ"}

	po := stationxml.Polynomial{
		BaseFilter: stationxml.BaseFilter{
			InputUnit:  inputUnit,
			OutputUnit: outputUnit},
		ApproximationType:       "MACLAURIN",
		FrequencyLowerBound:     lowerFrequency,
		FrequencyUpperBound:     upperFrequency,
		ApproximationLowerBound: *s.PolynomialResponse.LowerBoundApproximation,
		ApproximationUpperBound: *s.PolynomialResponse.UpperBoundApproximation,
		MaximumError:            s.PolynomialResponse.MaximumAbsoluteError}

	// Add Coefficients
	for index, current := range s.Coefficients {
		var currentError *float64
		if s.CoefficientsError != nil {
			tmp := s.CoefficientsError[index]
			currentError = &tmp
		}
		po.Coefficient = append(po.Coefficient, stationxml.CreateCoefficient(index, current, currentError))
	}

	stage := stationxml.Stage{Number: nextStageIndex, Polynomial: &po}
	result = append(result, stage)
	return
}

//
// Sensitivity is not defined for PolynomialSensor,
// only need for Stqged interfqce compliance
//
func (s *PolynomialSensor) Sensitivity() float64 {
	return 0
}

func loadPolynomialSensor(respRoot string, sensorFilePath string, abstract AbstractSensor, jsonStr []byte) (result *PolynomialSensor, err error) {
	var lerr error
	polyResp := new(PolynomialResponse)
	lerr = json.Unmarshal(jsonStr, polyResp)
	if lerr != nil {
		err = fmt.Errorf("Error decoding %s: %s", sensorFilePath, lerr.Error())
	}
	result = &PolynomialSensor{abstract, *polyResp}
	lerr = result.loadInclude(respRoot)
	if lerr != nil {
		err = fmt.Errorf("Error when loading include %s from %s: %s",
			*result.Include, sensorFilePath, lerr.Error())
	}
	lerr = result.isValid()
	if lerr != nil {
		err = fmt.Errorf("Error in %s: %s", sensorFilePath, lerr.Error())
	}
	return
}

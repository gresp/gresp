package argo

import (
	"fmt"
	"strconv"
	"time"
)

// OptionValue define interface
type OptionValue interface {
	Set([]string) error // Set set value of this OptionValue according to given arguments string representation
	String() string     // String give the string representation of this OptionValue
}

//
// BoolValue is the boolean implementation of OptionValue interface
//
type BoolValue struct {
	value bool
}

// Set is OptionValue implementation
func (v *BoolValue) Set(s []string) error {
	if s[0] == "false" || s[0] == "False" || s[0] == "0" {
		v.value = false
	} else if s[0] == "true" || s[0] == "True" || s[0] == "1" {
		v.value = true
	} else {
		return fmt.Errorf("can't parse %s as boolean value", s)
	}
	return nil
}

// String is OptionValue implementation
func (v *BoolValue) String() string {
	return fmt.Sprintf("%v", v.value)
}

//
// IntValue is the int implementation of OptionValue interface
//
type IntValue struct {
	value int
}

// Set is OptionValue implementation
func (v *IntValue) Set(s []string) error {
	intVar, err := strconv.Atoi(s[0])
	if err == nil {
		v.value = intVar
	}
	return err
}

// String is OptionValue implementation
func (v *IntValue) String() string {
	return fmt.Sprintf("%d", v.value)
}

//
// CounterValue is a int counter implementation of OptionValue interface
//
type CounterValue struct {
	value int
}

// Set is OptionValue implementation
func (v *CounterValue) Set(s []string) error {
	v.value += 1
	return nil
}

// String is OptionValue implementation
func (v *CounterValue) String() string {
	return fmt.Sprintf("%d", v.value)
}

//
// FloatValue is the float64 implementation of OptionValue interface
//
type FloatValue struct {
	value float64
}

// Set is OptionValue implementation
func (v *FloatValue) Set(s []string) error {
	intVar, err := strconv.ParseFloat(s[0], 64)
	if err == nil {
		v.value = intVar
	}
	return err
}

// String is OptionValue implementation
func (v *FloatValue) String() string {
	return fmt.Sprintf("%f", v.value)
}

//
// StringValue is the string implementation of OptionValue interface
//
type StringValue struct {
	value string
}

// Set is OptionValue implementation
func (v *StringValue) Set(s []string) error {
	v.value = s[0]
	return nil
}

// String is OptionValue implementation
func (v *StringValue) String() string {
	return v.value
}

//
// TimeValue is the time.Time implementation of OptionValue interface
//
type TimeValue struct {
	value time.Time
}

// Set is OptionValue implementation
func (v *TimeValue) Set(s []string) (err error) {
	v.value, err = time.Parse(time.RFC3339, s[0])
	return err
}

// String is OptionValue implementation
func (v *TimeValue) String() string {
	return v.value.Format(time.RFC3339)
}

//
// DurationValue is the time.Duration implementation of OptionValue interface
//
type DurationValue struct {
	value time.Duration
}

// Set is OptionValue implementation
func (v *DurationValue) Set(s []string) (err error) {
	v.value, err = time.ParseDuration(s[0])
	return err
}

// String is OptionValue implementation
func (v *DurationValue) String() string {
	return v.value.String()
}

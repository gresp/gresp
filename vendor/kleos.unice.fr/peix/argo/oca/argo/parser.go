package argo

import (
	"fmt"
	"strings"
)

// Parser is a command line parser.
type Parser struct {
	haveOption
	haveMutualExclusionGroup

	// A basic help
	help string

	// Say if parser have help option
	helpOption *OptionDefinition
	// The stack of valid options
	validOptionStack [][]*OptionDefinition

	additionnalParameters []string // The array of additionnal parameters
}

// New create a new Parser
func New(help string) (result *Parser) {
	return &Parser{
		help: help,
	}
}

// Add help option
func (p *Parser) UsageFlag() *bool {
	flagValue := &BoolValue{}
	p.helpOption = newOption("h", "help", "Print usage", flagValue,
		false, false, 0)
	p.haveOption.options = append(p.haveOption.options, p.helpOption)
	return &(flagValue.value)
}

// findOptionInSlice search OptionDefinition in a slice according to given short/long option.
func (p *Parser) findOptionInSlice(sliceOption []*OptionDefinition, option string, short bool) (result *OptionDefinition) {
	for _, currentOption := range sliceOption {
		if short && currentOption.short == option {
			return currentOption
		} else if !short && currentOption.long == option {
			return currentOption
		}
	}
	return nil
}

// findOption search OptionDefinition in current valid OptionDefinition
func (p *Parser) findOption(option string, short bool) (result *OptionDefinition, err error) {

	for i := len(p.validOptionStack) - 1; i >= 0; i-- {
		result = p.findOptionInSlice(p.validOptionStack[i], option, short)
		if result != nil {
			p.validOptionStack = p.validOptionStack[0 : i+1]
			return
		}
	}
	err = fmt.Errorf("undefined option %s", option)
	return
}

// optionCombinations give the list of possible option combinations
// This method is used to check Parser configuration
func (p *Parser) optionCombinations() (result [][]OptionDefinition) {
	result = p.haveOption.optionCombinations()
	result = cartesianProduct(result, p.haveMutualExclusionGroup.optionCombinations())
	return
}

//
// checkCombination make some checks on a given option combination.
//
//  -- Check that all option not have both short and long option equal to ""
//  -- Check that there is not two options with same short or long option
//
func checkCombination(combination []OptionDefinition) (err error) {
	shortOptions := make(map[string]OptionDefinition)
	longOptions := make(map[string]OptionDefinition)

	for _, currentOption := range combination {
		if currentOption.short == "" && currentOption.long == "" {
			return fmt.Errorf("both short and long option is \"\" for (%s)", currentOption)
		}
		if option, ok := shortOptions[currentOption.short]; ok {
			return fmt.Errorf("option (%s) and (%s) share same short option", option, currentOption)
		}
		shortOptions[currentOption.short] = currentOption
		if option, ok := longOptions[currentOption.long]; ok {
			return fmt.Errorf("option (%s) and (%s) share same long option", option, currentOption)
		}
		longOptions[currentOption.long] = currentOption
	}
	return
}

//
// checkConfig check parameters options coherence
//
//   -- Check that there no valid option combination with two options sharing same short or long option
//   -- Verify that short or long option is not ""
//
func (p *Parser) checkConfig() error {

	// First get all possibles options combinations
	optionCombinations := p.optionCombinations()
	for _, currentCombination := range optionCombinations {
		checkCombination(currentCombination)
	}
	return nil
}

func (p *Parser) verifyConstraint() (err error) {
	if err = p.haveOption.verifyConstraints(); err != nil {
		return
	}
	if err = p.haveMutualExclusionGroup.verifyConstraints(); err != nil {
		return
	}
	return
}

// processOption apply optionDefinition Set method on given parameters.
//
// parameters can contains more elements than expected number of arguments
func (p *Parser) processOption(definition *OptionDefinition, parameters []string) (nbParameters int, err error) {
	// Set option value according to arguments
	if definition.nbArg != 0 {
		if len(parameters) < definition.nbArg {
			err = fmt.Errorf("option (%s) expect %d arguments and get only %d",
				definition, definition.nbArg, len(parameters))
			return
		}
		definition.optionValue.Set(parameters[:definition.nbArg])
		nbParameters = definition.nbArg
	} else {
		definition.optionValue.Set([]string{"true"})
	}

	// Now we add suboption to validOptionStack
	if len(definition.options) != 0 {
		p.validOptionStack = append(p.validOptionStack, definition.options)
	}

	// Set flag presence
	definition.present()
	return
}

//
// processLongOption process a given long option.
//
// option is the long option without '--' prefix (Ex: verbose)
// parameters is a string slice containing remaining cmdline
//
// This method return the number of consumed argument
func (p *Parser) processLongOption(option string, parameters []string) (nbParameters int, err error) {
	var optionDef *OptionDefinition

	optionDef, err = p.findOption(option, false)
	if err != nil {
		return
	}
	nbParameters, err = p.processOption(optionDef, parameters)

	return
}

//
// processShortOption process a given short option.
//
// option is the short option without '-' prefix (Ex: v)
// parameters is a string slice containing remaining cmdline
//
// This method return the number of consumed argument
func (p *Parser) processShortOption(option string, parameters []string, onlyFlag bool) (nbParameters int, err error) {
	var optionDef *OptionDefinition

	optionDef, err = p.findOption(option, true)
	if err != nil {
		return
	}
	if onlyFlag && optionDef.nbArg != 0 {
		err = fmt.Errorf("multi short option syntax only valid for option without argument (%s)", optionDef)
		return
	}
	nbParameters, err = p.processOption(optionDef, parameters)

	return
}

//
// Parse parse given cmdLine according to Parser configuration.
//
// This method begin by coherence verifications on Parser configuration.
// Next, the cmdLine is parsed
// To finish verification is made that all required option are presents.
//
// After the call to this method without error, all option value can be used.
func (p *Parser) Parse(cmdLine []string) (err error) {
	// Check if Parser configuration is valid
	if err = p.checkConfig(); err != nil {
		return
	}

	var firstLevelOption = make([]*OptionDefinition, len(p.haveOption.options))
	copy(firstLevelOption, p.haveOption.options)
	firstLevelOption = append(firstLevelOption, p.haveMutualExclusionGroup.firstLevelOption()...)
	p.validOptionStack = append(p.validOptionStack, firstLevelOption)
	for i := 0; i < len(cmdLine); i += 1 {
		currentOption := cmdLine[i]
		if len(currentOption) == 1 {
			// current option lenght is 1
			if currentOption[0] == '-' {
				return fmt.Errorf("invalid parameters -")
			}
			p.additionnalParameters = append(p.additionnalParameters, cmdLine[i])
			continue
		} else if len(currentOption) == 2 {
			// current option lenght is 2
			if currentOption[0] == '-' && currentOption[1] == '-' {
				// -- mark the end of possible option
				p.additionnalParameters = append(p.additionnalParameters, cmdLine[i+1:]...)
				return
			} else if currentOption[0] == '-' {
				// find short option
				var nbParameters int
				nbParameters, err = p.processShortOption(currentOption[1:], cmdLine[i+1:], false)
				if err != nil {
					return
				}
				i += nbParameters
			} else {
				// find additionnal parameters
				p.additionnalParameters = append(p.additionnalParameters, cmdLine[i])
			}
			continue
		} else {
			// current option lenght is > 2
			if currentOption[0] == '-' && currentOption[1] == '-' {
				var nbParameters int
				nbParameters, err = p.processLongOption(currentOption[2:], cmdLine[i+1:])
				if err != nil {
					return
				}
				i += nbParameters
				continue
			} else if cmdLine[i][0] == '-' {
				// short option
				for _, currentShort := range currentOption[1:] {
					_, err = p.processShortOption(string(currentShort), cmdLine[i+1:], true)
					if err != nil {
						return
					}
				}
				continue
			} else {
				// Additionnal parameters
				p.additionnalParameters = append(p.additionnalParameters, cmdLine[i])
				continue
			}
		}
	}
	if p.helpOption == nil || !p.helpOption.isPresent {
		if err = p.verifyConstraint(); err != nil {
			return
		}
	}
	return
}

//
// Usage give the options helps as string
//
func (p *Parser) Usage() (result string) {
	toJoin := make([]string, 0, len(p.options))
	toJoin = append(toJoin, p.help+"\n")
	for _, option := range p.options {
		toJoin = append(toJoin, option.Usage())
	}
	for _, exclusion := range p.optionMutualExclusionGroup {
		toJoin = append(toJoin, fmt.Sprintf("Exclusive option for %s group:\n", exclusion.Name()))
		for _, option := range exclusion.options {
			toJoin = append(toJoin, option.Usage())
		}
	}
	result = strings.Join(toJoin, "")
	return
}

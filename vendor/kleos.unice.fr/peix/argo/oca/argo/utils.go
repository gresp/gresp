package argo

func cartesianProduct(a1 [][]OptionDefinition, a2 [][]OptionDefinition) (result [][]OptionDefinition) {
	if len(a1) == 0 {
		return a2
	} else if len(a2) == 0 {
		return a1
	}
	for _, currentA1Combination := range a1 {
		for _, currentA2Combination := range a1 {
			currentConcat := make([]OptionDefinition, 0, len(currentA1Combination)+len(currentA2Combination))
			currentConcat = append(currentConcat, currentA1Combination...)
			currentConcat = append(currentConcat, currentA2Combination...)
			result = append(result, currentConcat)
		}
	}
	return
}

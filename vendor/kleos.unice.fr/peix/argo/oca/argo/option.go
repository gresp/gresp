package argo

import (
	"fmt"
	"strings"
)

// OptionDefinition is used to manage option.
// In particular it permet to add suboptions.
type OptionDefinition struct {
	haveOption
	haveMutualExclusionGroup
	short       string      // Short option
	long        string      // Long option
	help        string      // The help string of this optionDefinition
	optionValue OptionValue // The OptionValue associated to this optionDefinition
	required    bool        // Required flag o this optionDefinition
	multiple    bool        // Say if the flag can appears multiple times
	nbArg       int         // Nuber of argument of this optionDefinition
	isPresent   bool        // Say if this option is present in already parsed cmdline.

}

//
// newOption create a new OptionDefinition
//
func newOption(short, long string, help string, value OptionValue, required, multiple bool, nbArg int) *OptionDefinition {
	return &OptionDefinition{
		short:       short,
		long:        long,
		help:        help,
		optionValue: value,
		required:    required,
		multiple:    multiple,
		nbArg:       nbArg,
	}
}

// present set flag presence for this OptionDefinition
func (o *OptionDefinition) present() {
	o.isPresent = true
}

// verifyConstraints verify cmdline constraints.
func (o OptionDefinition) verifyConstraints() (err error) {
	if o.required && !o.isPresent {
		return fmt.Errorf("option %s is required", o)
	}

	if err = o.haveOption.verifyConstraints(); err != nil {
		return
	}
	if err = o.haveMutualExclusionGroup.verifyConstraints(); err != nil {
		return
	}
	return
}

// optionCombinations give all the valid combination for this OptionDefinition
func (o OptionDefinition) optionCombinations() (result [][]OptionDefinition) {
	result = append(result, []OptionDefinition{o})
	result = cartesianProduct(result, o.haveMutualExclusionGroup.optionCombinations())
	result = cartesianProduct(result, o.haveOption.optionCombinations())
	return
}

func (o OptionDefinition) Usage() (result string) {
	toJoin := make([]string, 0)
	justifySpace := strings.Repeat(" ", 30-(len(o.short)+len(o.long)))
	if o.short == "" {
		toJoin = append(toJoin, fmt.Sprintf("--%s%s%s", o.long, justifySpace[:len(justifySpace)-2], o.help))
	} else if o.long == "" {
		toJoin = append(toJoin, fmt.Sprintf("-%s%s%s", o.short, justifySpace[:len(justifySpace)-1], o.help))
	} else {
		toJoin = append(toJoin, fmt.Sprintf("-%s, --%s%s%s", o.short, o.long, justifySpace[:len(justifySpace)-5], o.help))
	}
	if !o.required {
		toJoin = append(toJoin, fmt.Sprintf(" [default: %s]", o.optionValue.String()))
	}
	toJoin = append(toJoin, "\n")
	for _, currentSubOption := range o.haveOption.options {
		toJoin = append(toJoin, "\t", currentSubOption.Usage())
	}
	return strings.Join(toJoin, "")
}

func (o OptionDefinition) String() string {
	result := ""
	if o.short != "" {
		result += fmt.Sprintf("-%s", o.short)
	}
	if o.long != "" {
		result += fmt.Sprintf(", --%s", o.long)
	}
	if !o.required {
		result += fmt.Sprintf(" [%s by default]", o.optionValue.String())
	}
	return result
}

package argo

import "fmt"

// MutualExclusionGroup define a set of options that are mutually exclusive.
type MutualExclusionGroup struct {
	haveOption
	required bool
	name     string
}

// SetName set the name of this mutual exclusion group
func (g *MutualExclusionGroup) SetName(name string) {
	g.name = name
}

// Name give the name of this mutual exclusion group
func (g *MutualExclusionGroup) Name() string {
	return g.name
}

// optionCombinations give the slice of option combinations
func (g *MutualExclusionGroup) optionCombinations() (result [][]OptionDefinition) {
	for _, currentOption := range g.options {
		result = append(result, currentOption.optionCombinations()...)
	}
	return
}

// verifyConstraints verify that:
//  - Only one option is selected
//  - At least one option is selected if the group is required
func (g *MutualExclusionGroup) verifyConstraints() (err error) {
	var selectedOption *OptionDefinition
	for _, currentOption := range g.haveOption.options {
		if currentOption.isPresent {
			// Verify suboptions requirement
			if err = currentOption.verifyConstraints(); err != nil {
				return fmt.Errorf("%s present and %s", currentOption, err.Error())
			}
			// Verify that the first selected option of this mutual exlusion group
			if selectedOption != nil {
				return fmt.Errorf("%s and %s are exclusive", selectedOption, currentOption)
			}
			// Save selected option
			selectedOption = currentOption
		}
	}
	if g.required && selectedOption == nil {
		return fmt.Errorf("required option is missing for exclusive group")
	}
	return
}

// haveMutualExclusionGroup implement mutual exlusion group management aspect.
type haveMutualExclusionGroup struct {
	optionMutualExclusionGroup []*MutualExclusionGroup
}

// MutualExclusionGroup create a new mutual exclusion option group.
func (p *haveMutualExclusionGroup) MutualExclusionGroup(required bool) (result *MutualExclusionGroup) {
	result = &MutualExclusionGroup{
		required: required,
	}
	p.optionMutualExclusionGroup = append(p.optionMutualExclusionGroup, result)
	return
}

// verifyConstraints verify mutual exclusion group constraints
func (p *haveMutualExclusionGroup) verifyConstraints() (err error) {
	for _, currentGroup := range p.optionMutualExclusionGroup {
		if err = currentGroup.verifyConstraints(); err != nil {
			return
		}
	}
	return
}

// optionCombinations give the list of option combination related to mutual exclusive group
func (p *haveMutualExclusionGroup) optionCombinations() (result [][]OptionDefinition) {
	for _, currentGroup := range p.optionMutualExclusionGroup {
		result = append(result, currentGroup.optionCombinations()...)
	}
	return
}

// firstLevelOption give the list of option related to mutual exclusion group
func (p *haveMutualExclusionGroup) firstLevelOption() (result []*OptionDefinition) {
	for _, currentGroup := range p.optionMutualExclusionGroup {
		result = append(result, currentGroup.haveOption.options...)
	}
	return
}

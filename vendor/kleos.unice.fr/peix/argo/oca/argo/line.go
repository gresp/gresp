package argo

import (
	"time"
)

// haveOption implement options management.
//
// This definition is hierarchical, each option is also a haveOption
// responsible to define sub options.
type haveOption struct {
	options []*OptionDefinition // The array of defined option
}

func (p *haveOption) verifyConstraints() (err error) {
	if len(p.options) == 0 {
		return
	}
	for _, currentOption := range p.options {
		if err = currentOption.verifyConstraints(); err != nil {
			return
		}
	}
	return
}

// optionCombinations give the combinations of possible options
func (p *haveOption) optionCombinations() (result [][]OptionDefinition) {
	if len(p.options) == 0 {
		return
	}
	result = p.options[0].optionCombinations()
	for i := 1; i < len(p.options); i++ {
		result = cartesianProduct(result, p.options[i].optionCombinations())
	}
	return
}

//
// FlagArg add a flag option.
//
// short and long represent respectively short and long option.
//
// The method return is asociated with flag boolean value.
//
func (p *haveOption) FlagArg(short, long, help string) (value *bool, option *OptionDefinition) {
	v := BoolValue{value: false}
	option = newOption(short, long, help, &v, false, false, 0)
	p.options = append(p.options, option)
	value = &(v.value)
	return
}

//
// BoolArg add a boolean option.
//
// short and long represent respectively short and long option.
// defaultValue is used as default value if not nil.
// required say if a value must be associated with this option.
// So if a default value is given the value of required is ignored.
//
// The method return is asociated with boolean value.
//
func (p *haveOption) BoolArg(short, long, help string, defaultValue *bool, required bool) (value *bool, option *OptionDefinition) {
	v := BoolValue{}
	if defaultValue != nil {
		v.value = *defaultValue
	}
	option = newOption(short, long, help, &v, required && defaultValue == nil, false, 1)
	p.options = append(p.options, option)
	value = &(v.value)
	return
}

//
// IntArg add an integer option.
//
// short and long represent respectively short and long option.
// defaultValue is used as default value if not nil.
// required say if a value must be associated with this option.
// So if a default value is given the value of required is ignored.
//
// The method return is asociated with int value.
//
func (p *haveOption) IntArg(short, long, help string, defaultValue *int, required bool) (value *int, option *OptionDefinition) {
	v := IntValue{}
	if defaultValue != nil {
		v.value = *defaultValue
	}
	option = newOption(short, long, help, &v, required && defaultValue == nil, false, 1)
	p.options = append(p.options, option)
	value = &(v.value)
	return
}

//
// CounterArg add a counter option.
//
// short and long represent respectively short and long option.
// defaultValue is used as default value if not nil.
// if default value is nil the counter init valie is 0.
//
// The method return is asociated with int value.
//
func (p *haveOption) CounterArg(short, long, help string, defaultValue *int) (value *int, option *OptionDefinition) {
	v := CounterValue{}
	if defaultValue != nil {
		v.value = *defaultValue
	}
	option = newOption(short, long, help, &v, false, true, 0)
	p.options = append(p.options, option)
	value = &(v.value)
	return
}

//
// FloatArg add a float option.
//
// short and long represent respectively short and long option.
// defaultValue is used as default value if not nil.
// required say if a value must be associated with this option.
// So if a default value is given the value of required is ignored.
//
// The method return is asociated with float64 value.
//
func (p *haveOption) FloatArg(short, long, help string, defaultValue *float64, required bool) (value *float64, option *OptionDefinition) {
	var v FloatValue
	if defaultValue != nil {
		v = FloatValue{value: *defaultValue}
	}

	option = newOption(short, long, help, &v, required && defaultValue == nil, false, 1)
	p.options = append(p.options, option)
	value = &(v.value)
	return
}

//
// StringArg add a string option.
//
// short and long represent respectively short and long option.
// defaultValue is used as default value if not nil.
// required say if a value must be associated with this option.
// So if a default value is given the value of required is ignored.
//
// The method return is asociated with string value.
//
func (p *haveOption) StringArg(short, long, help string, defaultValue *string, required bool) (value *string, option *OptionDefinition) {
	v := StringValue{}
	if defaultValue != nil {
		v = StringValue{value: *defaultValue}
	}
	option = newOption(short, long, help, &v, required && defaultValue == nil, false, 1)
	p.options = append(p.options, option)
	value = &(v.value)
	return
}

//
// TimeArg add a time option.
//
// short and long represent respectively short and long option.
// defaultValue is used as default value if not nil.
// required say if a value must be associated with this option.
// So if a default value is given the value of required is ignored.
//
// The method return is asociated with time.Time value.
//
func (p *haveOption) TimeArg(short, long, help string, defaultValue *time.Time, required bool) (value *time.Time, option *OptionDefinition) {
	v := TimeValue{}
	if defaultValue != nil {
		v = TimeValue{value: *defaultValue}
	}
	option = newOption(short, long, help, &v, required && defaultValue == nil, false, 1)
	p.options = append(p.options, option)
	value = &(v.value)
	return
}

//
// DurationArg add a time option.
//
// short and long represent respectively short and long option.
// defaultValue is used as default value if not nil.
// required say if a value must be associated with this option.
// So if a default value is given the value of required is ignored.
//
// The method return is asociated with time.Duration value.
//
func (p *haveOption) DurationArg(short, long, help string, defaultValue *time.Duration, required bool) (value *time.Duration, option *OptionDefinition) {
	v := DurationValue{}
	if defaultValue != nil {
		v = DurationValue{value: *defaultValue}
	}
	option = newOption(short, long, help, &v, required && defaultValue == nil, false, 1)
	p.options = append(p.options, option)
	value = &v.value
	return
}

//
// Arg add a generic option.
//
// short and long represent respectively short and long option.
// value must implement OptionValue interface.
// required say if this option must be present.
// multiple say if this option can appear multiple times
// nbArg say how many parameters is consumed by this option.
//
func (p *haveOption) Arg(short, long, help string, value OptionValue, required, multiple bool, nbArg int) {
	option := newOption(short, long, help, value, required, multiple, nbArg)
	p.options = append(p.options, option)
}

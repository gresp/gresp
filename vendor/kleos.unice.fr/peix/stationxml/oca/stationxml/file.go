package stationxml

import (
	"encoding/xml"
)

// FDSNStationXML define station XML file
type FDSNStationXML struct {
	XMLName xml.Name `xml:"FDSNStationXML"`
	//Text          string    `xml:",chardata"`
	Xsi           string    `xml:"xmlns:xsi,attr,omitempty"`
	Xmlns         string    `xml:"xmlns,attr"`
	SchemaVersion string    `xml:"schemaVersion,attr"`
	Source        string    `xml:"Source"`
	Sender        string    `xml:"Sender,omitempty"`
	Module        string    `xml:"Module,omitempty"`
	ModuleURI     string    `xml:"ModuleURI,omitempty"`
	Created       string    `xml:"Created,omitempty"`
	Network       []Network `xml:"Network"`
}

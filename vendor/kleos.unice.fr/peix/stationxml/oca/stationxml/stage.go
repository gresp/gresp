package stationxml

// BaseFilter is derived by all filters
type BaseFilter struct {
	Description string `xml:"Description,omitempty"`
	InputUnit   Unit   `xml:"InputUnits"`
	OutputUnit  Unit   `xml:"OutputUnits"`
	ResourceID  string `xml:"resouceId,attr,omitempty"`
	Name        string `xml:"name,attr,omitempty"`
}

//
// PoleZero define one pole/zero value
type PoleZero struct {
	Number    int         `xml:"number,attr"`
	Real      FloatNoUnit `xml:"Real"`
	Imaginary FloatNoUnit `xml:"Imaginary"`
}

//
// PolesZeros define poles and zeros stages
//
type PolesZeros struct {
	BaseFilter
	PzTransferFunctionType string     `xml:"PzTransferFunctionType"`
	NormalizationFactor    float64    `xml:"NormalizationFactor"`
	NormalizationFrequency float64    `xml:"NormalizationFrequency"`
	Zero                   []PoleZero `xml:"Zero"`
	Pole                   []PoleZero `xml:"Pole"`
}

//
// Polynomial define global channel sensitivity for  polynomial channel
//
type Polynomial struct {
	BaseFilter
	ApproximationType       string        `xml:"ApproximationType"`
	FrequencyLowerBound     Frequency     `xml:"FrequencyLowerBound"`
	FrequencyUpperBound     Frequency     `xml:"FrequencyUpperBound"`
	ApproximationLowerBound float64       `xml:"ApproximationLowerBound"`
	ApproximationUpperBound float64       `xml:"ApproximationUpperBound"`
	MaximumError            float64       `xml:"MaximumError"`
	Coefficient             []Coefficient `xml:"Coefficient"`
}

//
// Coefficients define coefficient response
//
type Coefficients struct {
	BaseFilter
	CfTransferFunctionType string        `xml:"CfTransferFunctionType"`
	Numerator              []Coefficient `xml:"Numerator"`
	Denominator            []Coefficient `xml:"Denominator"`
}

//
// Fir define FIR response
//
type Fir struct {
	BaseFilter
	Symmetry             string `xml:"Symmetry"`
	NumeratorCoefficient []struct {
		Value float64 `xml:",chardata"`
		Index int     `xml:"i,attr"`
	} `xml:"NumeratorCoefficient"`
}

//
// Decimation define stage decimation
//
type Decimation struct {
	InputSampleRate Float `xml:"InputSampleRate"`
	Factor          int   `xml:"Factor"`
	Offset          int   `xml:"Offset"`
	Delay           Float `xml:"Delay"`
	Correction      Float `xml:"Correction"`
}

//
// Stage define response stage
//
type Stage struct {
	Number       int           `xml:"number,attr"`
	PolesZeros   *PolesZeros   `xml:"PolesZeros,omitempty"`
	Polynomial   *Polynomial   `xml:"Polynomial,omitempty"`
	Coefficients *Coefficients `xml:"Coefficients,omitempty"`
	FIR          *Fir          `xml:"FIR,omitempty"`
	Decimation   *Decimation   `xml:"Decimation,omitempty"`
	StageGain    *Gain         `xml:"StageGain,omitempty"`
}

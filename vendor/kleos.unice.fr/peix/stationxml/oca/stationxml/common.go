package stationxml

import "time"

const (
	// DateFormat define staion XML date format
	DateFormat = "2006-01-02T15:04:05Z"
)

// Identifier to document persistent identifiers.
// Identifier values should be specified without a URI scheme (prefix),
// instead the identifer type is documented as an attribute
type Identifier struct {
	Name string `xml:",chardata"`
	Type string `xml:"type,attr"`
}

// BaseNode define base type of Network, Station and Channel
type BaseNode struct {
	Description string      `xml:"Description,omitempty"`
	Identifier  *Identifier `xml:"Identifier,omitempty"`
	Comment     []Comment   `xml:"Comment,omitempty"`

	Code             string `xml:"code,attr"`
	StartDate        string `xml:"startDate,attr"`
	EndDate          string `xml:"endDate,attr,omitempty"`
	SourceID         string `xml:"sourceID,attr,omitempty"`
	RestrictedStatus string `xml:"restrictedStatus,attr,omitempty"`
	AlternateCode    string `xml:"alternateCode,attr,omitempty"`
	HistoricalCode   string `xml:"historicalCode,attr,omitempty"`
}

// Phone define stationxml PhoneType
type Phone struct {
	CountryCode *int   `xml:"CountryCode"`
	AreaCode    int    `xml:"AreaCode"`
	PhoneNumber string `xml:"PhoneNumber"`
}

// Person define stationxml PersonType
type Person struct {
	Name   []string `xml:"Name,omitempty"`
	Agency []string `xml:"Agency,omitempty"`
	Email  []string `xml:"Email,omitempty"`
	Phone  []Phone  `xml:"Phone,omitempty"`
}

// Comment define stationxml CommentType
type Comment struct {
	ID                 *int     `xml:"id,attr,omitempty"`
	Subject            string   `xml:"subject,attr,omitempty"`
	Value              string   `xml:"Value"`
	BeginEffectiveTime string   `xml:"BeginEffectiveTime,omitempty"`
	EndEffectiveTime   string   `xml:"EndEffectiveTime,omitempty"`
	Author             []Person `xml:"Author"`
}

// Unit define StationXML unit
type Unit struct {
	Name        string `xml:"Name"`
	Description string `xml:"Description"`
}

// FloatNoUnit definie float value with uncertainty
type FloatNoUnit struct {
	Value             float64  `xml:",chardata"`
	MinError          *float64 `xml:"plusError,attr,omitempty"`
	MaxError          *float64 `xml:"minusError,attr,omitempty"`
	MeasurementMethod *string  `xml:"measurementMethod,attr,omitempty"`
}

// Float define float with unit
type Float struct {
	FloatNoUnit
	Unit string `xml:"unit,attr,omitempty"`
}

// MakeFloat make a new Float
func MakeFloat(value float64, minError, maxError *float64, measurementMethod *string, unit string) Float {
	return Float{
		FloatNoUnit: FloatNoUnit{
			Value:             value,
			MinError:          minError,
			MaxError:          maxError,
			MeasurementMethod: measurementMethod},
		Unit: unit}
}

// Frequency define frequency (Always in Hertz)
type Frequency struct {
	FloatNoUnit
	Unit string `xml:"unit,attr"`
}

// Coefficient define coefficient
type Coefficient struct {
	FloatNoUnit
	Number int `xml:"number,attr"`
}

// CreateCoefficient create polynomial coefficient
func CreateCoefficient(index int, val float64, valError *float64) Coefficient {

	value := FloatNoUnit{
		Value:    val,
		MinError: valError,
		MaxError: valError}
	return Coefficient{
		FloatNoUnit: value,
		Number:      index}
}

// Equipement define equiment description
type Equipement struct {
	ResourceID       string     `xml:"resouceId,attr,omitempty"`
	Type             string     `xml:"Type,omitempty"`
	Description      string     `xml:"Description,omitempty"`
	Manufacturer     string     `xml:"Manufacturer,omitempty"`
	Vendor           string     `xml:"Vendor,omitempty"`
	Model            string     `xml:"Model,omitempty"`
	SerialNumber     string     `xml:"SerialNumber,omitempty"`
	InstallationDate *time.Time `xml:"InstallationDate,omitempty"`
	RemovalDate      *time.Time `xml:"RemovalDate,omitempty"`
	CalibrationDate  *time.Time `xml:"CalibrationDate,omitempty"`
}

// Site define station site description
type Site struct {
	Name        string `xml:"Name"`
	Description string `xml:"Description,omitempty"`
	Town        string `xml:"Town,omitempty"`
	County      string `xml:"County,omitempty"`
	Region      string `xml:"Region,omitempty"`
	Country     string `xml:"Country,omitempty"`
}

// PhoneNumber define phone number representation
type PhoneNumber struct {
	CountryCode int    `xml:"CountryCode"`
	AreaCode    int    `xml:"AreaCode"`
	PhoneNumber string `xml:"PhoneNumber"`
}

// Operator define station operator
type Operator struct {
	Agency  string  `xml:"Agency"`
	Contact *Person `xml:"Contact,omitempty"`
	WebSite string  `xml:"WebSite,omitempty"`
}

// ExternalReference contains a URI and description for external
// data that users may want to reference in StationXML.
type ExternalReference struct {
	URI         string `xml:"URI,omitempty"`
	Description string `xml:"Description,omitempty"`
}

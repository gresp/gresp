package stationxml

// Channel define a station channel description
type Channel struct {
	BaseNode
	ExternalReference []ExternalReference `xml:"ExternalReference,omitempty"`
	Latitude          Float               `xml:"Latitude"`
	Longitude         Float               `xml:"Longitude"`
	Elevation         Float               `xml:"Elevation"`
	Depth             Float               `xml:"Depth"`
	Azimuth           *Float              `xml:"Azimuth,omitempty"`
	Dip               *Float              `xml:"Dip,omitempty"`
	WaterLevel        *Float              `xml:"WaterLevel,omitempty"`
	Type              []string            `xml:"Type"`
	SampleRate        *Float              `xml:"SampleRate,omitempty"`
	ClockDrift        *Float              `xml:"ClockDrift,omitempty"`
	CalibrationUnits  *Unit               `xml:"CalibrationUnits,omitempty"`
	Sensor            *Equipement         `xml:"Sensor,omitempty"`
	PreAmplifier      *Equipement         `xml:"PreAmplifier,omitempty"`
	DataLogger        *Equipement         `xml:"DataLogger,omitempty"`
	Response          *Response           `xml:"Response"`
	LocationCode      string              `xml:"locationCode,attr"`
}

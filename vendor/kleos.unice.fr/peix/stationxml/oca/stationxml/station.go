package stationxml

// Station define station description
type Station struct {
	BaseNode
	Latitude               Float               `xml:"Latitude"`
	Longitude              Float               `xml:"Longitude"`
	Elevation              Float               `xml:"Elevation"`
	Site                   Site                `xml:"Site"`
	WaterLevel             *Float              `xml:"WaterLevel,omitempty"`
	Vault                  string              `xml:"Vault,omitempty"`
	Geology                string              `xml:"Geology,omitempty"`
	Equipement             []Equipement        `xml:"Equipement"`
	Operator               []Operator          `xml:"Operator,omitempty"`
	CreationDate           string              `xml:"CreationDate"`
	TerminationDate        string              `xml:"TerminationDate,omitempty"`
	TotalNumberChannels    int                 `xml:"TotalNumberChannels"`
	SelectedNumberChannels int                 `xml:"SelectedNumberChannels"`
	ExternalReference      []ExternalReference `xml:"ExternalReference,omitempty"`
	Channel                []Channel
}

package stationxml

//
// Network define station network description
//
type Network struct {
	BaseNode
	Operator               *Operator `xml:"Operator,omitempty"`
	TotalNumberOfStations  int       `xml:"TotalNumberOfStations,omitempty"`
	SelectedNumberStations int       `xml:"SelectedNumberStations,omitempty"`
	Station                []Station `xml:"Station"`
}

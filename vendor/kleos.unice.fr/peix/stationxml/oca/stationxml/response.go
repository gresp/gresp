package stationxml

import "math"

//
// Gain define stage gain
//
type Gain struct {
	Value     float64 `xml:"Value"`
	Frequency float64 `xml:"Frequency"`
}

//
// Sensitivity define global channel sensitivity for non polynomial channel
//
type Sensitivity struct {
	//	Text  string `xml:",chardata"`
	Gain
	InputUnits  Unit `xml:"InputUnits"`
	OutputUnits Unit `xml:"OutputUnits"`
}

//
// Response define channel response
//
type Response struct {
	InstrumentSensitivity *Sensitivity `xml:"InstrumentSensitivity,omitempty"`
	InstrumentPolynomial  *Polynomial  `xml:"InstrumentPolynomial,omitempty"`
	Stage                 []Stage
	ResourceID            string `xml:"resouceId,attr,omitempty"`
}

//
// UpdateSensitivity update full channel response stage with given sensitivity value
//
func (r *Response) UpdateSensitivity(sensitivity float64) {
	if r.InstrumentSensitivity != nil {
		r.InstrumentSensitivity.Gain.Value *= sensitivity
	} else if r.InstrumentPolynomial != nil {
		newCoef := make([]Coefficient, 0, len(r.InstrumentPolynomial.Coefficient))
		for index, currentCoef := range r.InstrumentPolynomial.Coefficient {
			// TODO: Verify if we must also update error
			newCoef = append(newCoef, CreateCoefficient(index, currentCoef.Value/math.Pow(sensitivity, float64(index)), currentCoef.MinError))
		}
		r.InstrumentPolynomial.Coefficient = newCoef
	} else {
		panic("error")
	}
}

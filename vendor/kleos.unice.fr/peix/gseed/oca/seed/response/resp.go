package response

import (
	"fmt"
	"math"
	"math/cmplx"
	"strings"
)

//
// analogTrans compute response of analog filter at a given frequency.
//
// zeroes     -- The slice of zeroes
// poles      -- The slice of poles
// freq       -- The frequency at which response is computed
// h0         -- The value of Sd * A0
// filterType -- The filter type (LAPLACE|ANALOG)
//
// return     -- The response for requested frequency
//
func analogTrans(zeroes, poles []complex128, freq float64, h0 float64, filterType string) complex128 {
	var denom, num, omega, temp complex128

	if strings.Compare(filterType, "LAPLACE") != 0 && strings.Compare(filterType, "ANALOG") != 0 {
		panic(fmt.Sprintf("The filter type is %s and must be LAPLACE | ANALOG", filterType))
	}

	if strings.Compare(filterType, "LAPLACE") == 0 {
		freq = 2 * math.Pi * freq
	}
	omega = complex(0, freq)
	denom = complex(1, 0)
	num = complex(1, 0)

	for _, currentZero := range zeroes {
		/* num=num*(omega-zero[i]) */
		temp = omega - currentZero
		num *= temp
	}
	for _, currentPole := range poles {
		temp = omega - currentPole
		denom *= temp
	}

	return (num / denom) * complex(h0, 0)
}

//
// iirPzTrans compute response of IIR Pz digital filter at a given frequency.
//
// zeroes     -- The slice of zeroes
// poles      -- The slice of poles
// freq       -- The frequency at which response is computed
// sint       -- The sampling interval
//
// return     -- The response for requested frequency
//
func iirPzTrans(zeroes, poles []complex128, freq float64, sint float64) complex128 {
	var numerator = complex(1, 0)
	var denominator = complex(1, 0)

	zn := complex(math.Cos(2*math.Pi*freq*sint), math.Sin(2*math.Pi*freq*sint))
	//for index, currentZero := range zeroes {
	//	fmt.Printf("numerator (%d) = %v\n", index, numerator)
	//	numerator *= (zn - currentZero)
	//}
	// Change iteration procedure to reduce chance to produce
	// to small value out of machine representation.

	// Iterate on even indexed values
	for i := 0; i < len(zeroes); i += 2 {
		numerator *= (zn - zeroes[i])
	}
	// Iterate on odd indexed values
	for i := 1; i < len(zeroes); i += 2 {
		numerator *= (zn - zeroes[i])
	}
	for _, currentPole := range poles {
		denominator *= (zn - currentPole)
	}
	return numerator / denominator
}

//
// CalcAnalogA0 compute A0 for analog filter at a given frequency.
//
// zeroes     -- The slice of zeroes
// poles      -- The slice of poles
// fn         -- The frequency at which response is computed
// filterType -- The filter type (LAPLACE|ANALOG)
//
// return     -- The A0 for requested frequency
//
func CalcAnalogA0(zeroes []complex128, poles []complex128, fn float64, filterType string) float64 {
	return 1.0 / cmplx.Abs(analogTrans(zeroes, poles, fn, 1.0, filterType))
}

//
// CalcIirPzA0 compute A0 for IIR Pz digital filter at a given frequency.
//
// zeroes -- The slice of zeroes
// poles  -- The slice of poles
// freq   -- The frequency at which A0 is computed
// sint   -- The sampling interval
//
// return -- The A0 for requested frequency
//
func CalcIirPzA0(zeroes []complex128, poles []complex128, fn float64, sint float64) float64 {
	return 1.0 / cmplx.Abs(iirPzTrans(zeroes, poles, fn, sint))
}

//
// iirCoefTrans compute response of coefficient digital filter at a given frequency.
//
// zeroes     -- The slice of zeroes
// poles      -- The slice of poles
// freq       -- The frequency at which response is computed
// sint       -- The sampling interval
//
// return     -- The response for requested frequency
//
func iirCoefTrans(numerator, denominator []float64, wint float64, sampleInterval float64) complex128 {
	/* Calculate radial freq. time sample interval */
	w := wint * sampleInterval

	tempNum := complex(numerator[0], 0)

	/* handle numerator */
	for index, currentNum := range numerator[1:] {
		tempNum += complex(currentNum*math.Cos(-float64(index)*w),
			currentNum*math.Sin(-float64(index)*w))
	}
	/* handle denominator */
	tempDen := complex(denominator[0], 0)
	for index, currentDen := range denominator[1:] {
		tempDen += complex(currentDen*math.Cos(-float64(index)*w),
			currentDen*math.Sin(-float64(index)*w))
	}

	return tempNum / tempDen
}

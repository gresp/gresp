package main

import (
	"encoding/xml"
	"fmt"
	"os"
	"path"

	"kleos.unice.fr/peix/argo/oca/argo"
	"kleos.unice.fr/peix/gdbird/oca/dbirdv2"
	"kleos.unice.fr/peix/gdbird/oca/ydbird"
)

func main() {

	defaultOutputFile := "output.xml"

	// Command line management
	parser := argo.New("Gresp a stationXML generator by RESIF OCA team")

	ydbirdFile, _ := parser.StringArg("y", "ydbird", "YAML dbird file input file", nil, true)
	pzPath, _ := parser.StringArg("", "pz", "Set the PZ databank root directory", nil, true)
	schemaRootDir, _ := parser.StringArg("", "schema", "Set the root schema directory", nil, true)
	xmlFile, _ := parser.StringArg("o", "output", "Set the name of stationXML output file", &defaultOutputFile, false)
	obspyBug, _ := parser.FlagArg("", "obspyPolyBug", "Generate polynomial response according to Obspy Buggy implementation")
	indent, _ := parser.FlagArg("", "indent", "Enable output indent")
	verboseFlag, _ := parser.FlagArg("v", "verbose", "Set verbose flag")
	usageFlag := parser.UsageFlag()

	// Parse command line
	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Printf("Error: %s", err)
		os.Exit(1)
	}

	// Print usage if need
	if *usageFlag {
		fmt.Print(parser.Usage())
		os.Exit(0)
	}

	// Loading sensor_file
	if *verboseFlag {
		fmt.Printf("Loading sensor_file\n")
	}
	sensorList, serr := dbirdv2.LoadSensorFile(path.Join(*pzPath, "sensor_list"),
		path.Join(*schemaRootDir, "sensor_list.json"))
	if serr != nil {
		fmt.Printf("Error loading sensor_file: %s\n", serr.Error())
		os.Exit(1)
	}

	dasList, derr := dbirdv2.LoadDasFile(path.Join(*pzPath, "das_list"),
		path.Join(*schemaRootDir, "das_list.json"))
	if derr != nil {
		fmt.Printf("Error loading das_file: %s\n", derr.Error())
		os.Exit(1)
	}

	// Loading unit_file
	if *verboseFlag {
		fmt.Printf("Loading unit_file\n")
	}
	unitList, uerr := dbirdv2.LoadUnitFile(path.Join(*pzPath, "unit_list"),
		path.Join(*schemaRootDir, "unit_list.json"))
	if uerr != nil {
		fmt.Printf("Error loading unit_file: %s\n", uerr.Error())
		os.Exit(1)
	}

	var dbirdFile dbirdv2.Dbird
	var lerr error
	if *verboseFlag {
		fmt.Printf("Loading ydbird file %s\n", *ydbirdFile)
	}
	dbirdFile, lerr = ydbird.Load(*ydbirdFile, *pzPath)
	if lerr != nil {
		fmt.Printf("Error loading dbird file: %s\n", lerr.Error())
		os.Exit(1)
	}

	// Generate stationXML if need
	converter, err := dbirdv2.NewStationXMLConverter(sensorList, dasList, unitList,
		*schemaRootDir, *obspyBug)
	if err != nil {
		fmt.Printf("Error creating converter: %s\n", err.Error())
		os.Exit(1)
	}
	xmlContent, errorList := converter.Convert(dbirdFile)

	for _, current := range errorList {
		fmt.Printf("%s\n", current.Error())
	}

	f, err := os.Create(*xmlFile)
	if err != nil {
		fmt.Printf("Error opening output file %s: %s", *xmlFile, err.Error())
		os.Exit(1)
	}
	defer f.Close()
	var output []byte
	if *indent {
		output, err = xml.MarshalIndent(xmlContent, "", "  ")
	} else {
		output, err = xml.Marshal(xmlContent)
	}
	if err != nil {
		fmt.Printf("Error generating XML: %s", err.Error())
		os.Exit(1)
	}
	f.Write([]byte("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"))
	_, err = f.Write([]byte(output))
	if err != nil {
		fmt.Printf("Error writing output file %s: %s", *xmlFile, err.Error())
		os.Exit(1)
	}
	if errorList != nil {
		os.Exit(1)
	}
	os.Exit(0)

}

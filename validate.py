#!/usr/bin/env python3
import os
import sys
import json
import yaml
import argparse
from jsonschema import Draft6Validator

def load_file(filename, loader):
    if not os.path.exists(filename):
        sys.stderr.write("The input file %s does not exists\n" %
                         filename)
        sys.exit(1)

    try:
        config_file = open(filename)
        to_validate = loader(config_file)
    except IOError as exception:
        sys.stderr.write("Error opening %s: %s\n" %
                         (filename, exception))
        sys.exit(1)
    except ValueError as exception:
        sys.stderr.write("Error parsing YAML in %s: %s\n" %
                         (filename, exception))
        sys.exit(1)
    return to_validate


def main():
    parser = argparse.ArgumentParser(description='Validate dbird YAML with a given JSON schema')

    parser.add_argument("-y", "--yaml", help="Set YAML file to validate", required=True)
    parser.add_argument("--schema", help="Set schema file used to validate")
    parser.add_argument("-v", "--verbose", action="count", help="Increase verbosity level", default=0)

    option = parser.parse_args(sys.argv[1:])

    # Load file to validate
    to_validate = load_file(option.yaml, yaml.safe_load)

    # Load schema
    if not os.path.exists(option.schema):
        sys.stderr.write("The schema file %s does not exists\n" %
                         option.schema)
        sys.exit(1)
    try:
        schema_file = open(option.schema)
        schema = json.load(schema_file)
    except IOError as exception:
        sys.stderr.write("Error opening %s: %s\n" %
                         (option.schema, exception))
        sys.exit(1)
    except ValueError as exception:
        sys.stderr.write("Error parsing JSON in %s: %s\n" %
                         (option.schema, exception))
        sys.exit(1)
    Draft6Validator.check_schema(schema)
    # Make validation
    Draft6Validator(schema).validate(to_validate)


if __name__ == "__main__":
    main()

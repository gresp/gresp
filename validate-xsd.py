#!/usr/bin/env python
import os
import sys
import argparse
from lxml import etree


def validate(xml_path, xsd_path):
    """
    """
    xmlschema_doc = etree.parse(xsd_path)
    xmlschema = etree.XMLSchema(xmlschema_doc)

    xml_doc = etree.parse(xml_path)
    xmlschema.assertValid(xml_doc)



def main():
    parser = argparse.ArgumentParser(description='Validate an XML file with a XSD schema')

    parser.add_argument("-f", "--file", help="Set the XML filepath", required=True)
    parser.add_argument("-s", "--schema", help="Set the XSD schema filepath", default=None)
    parser.add_argument("-v", "--verbose", action="count", help="Increase verbosity level", default=0)

    option = parser.parse_args(sys.argv[1:])
    if not os.path.exists(option.file):
        sys.stderr.write("The XML file %s does not exists\n" %
                         option.file)
        sys.exit(1)

    if not os.path.exists(option.schema):
        sys.stderr.write("The XSD schema file %s does not exists\n" %
                         option.schema)
        sys.exit(1)
    validate(option.file, option.schema)


if __name__ == "__main__":
    main()
